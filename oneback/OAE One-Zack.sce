# -------------------------- Header Parameters --------------------------
scenario = "ORE & Emotion";
write_codes = EXPARAM( "Send Port Codes" );
screen_width_distance = EXPARAM( "Display Width" );
screen_height_distance = EXPARAM( "Display Height" );
screen_distance = EXPARAM( "Viewing Distance" );
default_background_color = EXPARAM( "Background Color" );
default_font = EXPARAM( "Non-Stimulus Font" );
default_font_size = EXPARAM( "Non-Stimulus Font Size" );
default_text_color = EXPARAM( "Non-Stimulus Font Color" );
active_buttons = 1;
response_matching = simple_matching;
target_button_codes = 1;
response_logging = EXPARAM( "Response Logging" );
stimulus_properties =
	event_name, string,
	prac_type, string,
	trial_number, number,
	task_type, string, # need to declare this here??
	stim_type, string,
	stim_age, string,
	stim_set, string,
	stim_emotion, string,
	stim_model_num, string,
	ISI_duration, number,
	p_code, number,
	stim_file, string;
event_code_delimiter = ";";
# ------------------------------- SDL Part ------------------------------
begin;
# declares tone wavefile
# wavefile { filename = "ding.wav"; } tone;
trial{
	trial_type = first_response;
	trial_duration = forever;
	picture{
		plane { width=0.0; height=0.0; emissive=1.0,1.0,1.0; } stimulus_holder_plane;
                       x=0; y=0; z=0;
		text { 
			caption = "Instructions";
			preload = false;
		} instruct_text;
		x = 0; 
		y = 0;
	} instruct_pic;
} instruct_trial;
trial {
	stimulus_event {
		picture {} ISI_pic;
		code = "ISI";
	} ISI_event;
} ISI_trial;
trial {
	clear_active_stimuli = false;
	stimulus_event {
		picture {
			ellipse_graphic {
				ellipse_height = EXPARAM( "Fixation Point Size" );
				ellipse_width = EXPARAM( "Fixation Point Size" );
				color = EXPARAM( "Fixation Point Color" );
			} fix_circ;
			x = 0;
			y = 0;
		} stim_pic;
		response_active = true;
		code = "Stim";
	} stim_event;
	#sound { wavefile tone;} sound1;
} stim_trial;
trial {
	trial_duration = forever;
	trial_type = first_response;
	
	stimulus_event {
		picture {
			text {
				caption = "Rest";
				preload = false;
			} rest_text;
			x = 0;
			y = 0;
		};
		code = "Rest";
	} rest_event;
} rest_trial;
trial {
	stimulus_event {
		picture {
			text {
				caption = "Ready";
				preload = false;
			} ready_text;
			x = 0;
			y = 0;
		};
	} ready_event;
} ready_trial;
# note-: I will need to add to stim_trial
# note-: sound1.set_attenuation(1); <== causes silence
# note-: sound1.set_attenuation(0); <== causes normal sound of wavefile

#bitmap A001 = new bitmap("001-A.bmp");
#bitmap F001 = new bitmap("001-F.bmp");
# Young
bitmap{ filename = "YF_P041_ANG.bmp"; } A001;
bitmap{ filename = "YF_P041_FEA.bmp"; } F001;
bitmap{ filename = "YF_P041_NEU.bmp"; } N001;
bitmap{ filename = "YF_P072_ANG.bmp"; } A002;
bitmap{ filename = "YF_P072_FEA.bmp"; } F002;
bitmap{ filename = "YF_P072_NEU.bmp"; } N002;	
bitmap{ filename = "YF_P119_ANG.bmp"; } A003;
bitmap{ filename = "YF_P119_FEA.bmp"; } F003;
bitmap{ filename = "YF_P119_NEU.bmp"; } N003;
# ------------------------------------------------------ #
# Old
bitmap{ filename = "OF_P046_ANG.bmp"; } A004;
bitmap{ filename = "OF_P046_FEA.bmp"; } F004;
bitmap{ filename = "OF_P046_NEU.bmp"; } N004;
bitmap{ filename = "OF_P059_ANG.bmp"; } A005;
bitmap{ filename = "OF_P059_FEA.bmp"; } F005;
bitmap{ filename = "OF_P059_NEU.bmp"; } N005;
bitmap{ filename = "OF_P176_ANG.bmp"; } A006;  
bitmap{ filename = "OF_P176_FEA.bmp"; } F006;
bitmap{ filename = "OF_P176_NEU.bmp"; } N006;
# Scrambled Young
bitmap{ filename = "YT_P041_ANG.bmp"; } SCRAMBLED1;
bitmap{ filename = "YT_P041_FEA.bmp"; } SCRAMBLED2;
bitmap{ filename = "YT_P041_NEU.bmp"; } SCRAMBLED3;
bitmap{ filename = "YT_P072_ANG.bmp"; } SCRAMBLED4;
bitmap{ filename = "YT_P072_FEA.bmp"; } SCRAMBLED5;
bitmap{ filename = "YT_P072_NEU.bmp"; } SCRAMBLED6;
bitmap{ filename = "YT_P119_ANG.bmp"; } SCRAMBLED7;
bitmap{ filename = "YT_P119_FEA.bmp"; } SCRAMBLED8;
bitmap{ filename = "YT_P119_NEU.bmp"; } SCRAMBLED9;
# Scrambled Old ------------------------------------------------------ # 
bitmap{ filename = "OT_P046_ANG.bmp"; } SCRAMBLED10;
bitmap{ filename = "OT_P046_FEA.bmp"; } SCRAMBLED11;
bitmap{ filename = "OT_P046_NEU.bmp"; } SCRAMBLED12;
bitmap{ filename = "OT_P059_ANG.bmp"; } SCRAMBLED13;
bitmap{ filename = "OT_P059_FEA.bmp"; } SCRAMBLED14;
bitmap{ filename = "OT_P059_NEU.bmp"; } SCRAMBLED15;
bitmap{ filename = "OT_P176_ANG.bmp"; } SCRAMBLED16;
bitmap{ filename = "OT_P176_FEA.bmp"; } SCRAMBLED17;
bitmap{ filename = "OT_P176_NEU.bmp"; } SCRAMBLED18;
# Old SET A
bitmap{ filename = "OF_A015_ANG.bmp"; } A1;
bitmap{ filename = "OF_A015_NEU.bmp"; } N1;
bitmap{ filename = "OF_A033_ANG.bmp"; } A2;
bitmap{ filename = "OF_A033_NEU.bmp"; } N2;
bitmap{ filename = "OF_A091_ANG.bmp"; } A3;
bitmap{ filename = "OF_A091_NEU.bmp"; } N3;
bitmap{ filename = "OF_A131_ANG.bmp"; } A4;
bitmap{ filename = "OF_A131_NEU.bmp"; } N4;
bitmap{ filename = "OF_A004_ANG.bmp"; } A5;
bitmap{ filename = "OF_A004_FEA.bmp"; } F5;
bitmap{ filename = "OF_A083_ANG.bmp"; } A6;
bitmap{ filename = "OF_A083_FEA.bmp"; } F6;
bitmap{ filename = "OF_A107_ANG.bmp"; } A7;
bitmap{ filename = "OF_A107_FEA.bmp"; } F7;
bitmap{ filename = "OF_A118_ANG.bmp"; } A8;
bitmap{ filename = "OF_A118_FEA.bmp"; } F8;
bitmap{ filename = "OF_A027_FEA.bmp"; } F9;
bitmap{ filename = "OF_A027_NEU.bmp"; } N9;
bitmap{ filename = "OF_A065_FEA.bmp"; } F10;
bitmap{ filename = "OF_A065_NEU.bmp"; } N10;
bitmap{ filename = "OF_A146_FEA.bmp"; } F11;
bitmap{ filename = "OF_A146_NEU.bmp"; } N11;
bitmap{ filename = "OF_A151_FEA.bmp"; } F12;
bitmap{ filename = "OF_A151_NEU.bmp"; } N12;
# Scrambled
bitmap{ filename = "OT_A015_ANG.bmp"; } TOFU1;
bitmap{ filename = "OT_A015_NEU.bmp"; } TOFU2;
bitmap{ filename = "OT_A033_ANG.bmp"; } TOFU3;
bitmap{ filename = "OT_A033_NEU.bmp"; } TOFU4;
bitmap{ filename = "OT_A091_ANG.bmp"; } TOFU5;
bitmap{ filename = "OT_A091_NEU.bmp"; } TOFU6;
bitmap{ filename = "OT_A131_ANG.bmp"; } TOFU7;
bitmap{ filename = "OT_A131_NEU.bmp"; } TOFU8; 
bitmap{ filename = "OT_A004_ANG.bmp"; } TOFU9;
bitmap{ filename = "OT_A004_FEA.bmp"; } TOFU10;
bitmap{ filename = "OT_A083_ANG.bmp"; } TOFU11;
bitmap{ filename = "OT_A083_FEA.bmp"; } TOFU12;
bitmap{ filename = "OT_A107_ANG.bmp"; } TOFU13;
bitmap{ filename = "OT_A107_FEA.bmp"; } TOFU14;
bitmap{ filename = "OT_A118_ANG.bmp"; } TOFU15;
bitmap{ filename = "OT_A118_FEA.bmp"; } TOFU16;
bitmap{ filename = "OT_A027_FEA.bmp"; } TOFU17;
bitmap{ filename = "OT_A027_NEU.bmp"; } TOFU18;
bitmap{ filename = "OT_A065_FEA.bmp"; } TOFU19;
bitmap{ filename = "OT_A065_NEU.bmp"; } TOFU20;
bitmap{ filename = "OT_A146_FEA.bmp"; } TOFU21;
bitmap{ filename = "OT_A146_NEU.bmp"; } TOFU22;
bitmap{ filename = "OT_A151_FEA.bmp"; } TOFU23;
bitmap{ filename = "OT_A151_NEU.bmp"; } TOFU24;
	
# ------ Old Set B ------ #
bitmap{ filename = "OF_B015_ANG.bmp"; } A13;
bitmap{ filename = "OF_B015_NEU.bmp"; } N13;
bitmap{ filename = "OF_B033_ANG.bmp"; } A14;
bitmap{ filename = "OF_B033_NEU.bmp"; } N14;
bitmap{ filename = "OF_B091_ANG.bmp"; } A15;
bitmap{ filename = "OF_B091_NEU.bmp"; } N15;
bitmap{ filename = "OF_B131_ANG.bmp"; } A16;
bitmap{ filename = "OF_B131_NEU.bmp"; } N16;
bitmap{ filename = "OF_B004_ANG.bmp"; } A17;
bitmap{ filename = "OF_B004_FEA.bmp"; } F17;
bitmap{ filename = "OF_B083_ANG.bmp"; } A18;
bitmap{ filename = "OF_B083_FEA.bmp"; } F18;
bitmap{ filename = "OF_B107_ANG.bmp"; } A19;
bitmap{ filename = "OF_B107_FEA.bmp"; } F19;
bitmap{ filename = "OF_B118_ANG.bmp"; } A20;
bitmap{ filename = "OF_B118_FEA.bmp"; } F20;
bitmap{ filename = "OF_B027_FEA.bmp"; } F21;
bitmap{ filename = "OF_B027_NEU.bmp"; } N21;
bitmap{ filename = "OF_B065_FEA.bmp"; } F22;
bitmap{ filename = "OF_B065_NEU.bmp"; } N22;
bitmap{ filename = "OF_B146_FEA.bmp"; } F23;
bitmap{ filename = "OF_B146_NEU.bmp"; } N23;
bitmap{ filename = "OF_B151_FEA.bmp"; } F24;
bitmap{ filename = "OF_B151_NEU.bmp"; } N24;
# Scrambled
bitmap{ filename = "OT_B015_ANG.bmp"; } TOFU25;
bitmap{ filename = "OT_B015_NEU.bmp"; } TOFU26;
bitmap{ filename = "OT_B033_ANG.bmp"; } TOFU27;
bitmap{ filename = "OT_B033_NEU.bmp"; } TOFU28;
bitmap{ filename = "OT_B091_ANG.bmp"; } TOFU29;
bitmap{ filename = "OT_B091_NEU.bmp"; } TOFU30;
bitmap{ filename = "OT_B131_ANG.bmp"; } TOFU31;
bitmap{ filename = "OT_B131_NEU.bmp"; } TOFU32;
bitmap{ filename = "OT_B004_ANG.bmp"; } TOFU33;
bitmap{ filename = "OT_B004_FEA.bmp"; } TOFU34;
bitmap{ filename = "OT_B083_ANG.bmp"; } TOFU35;
bitmap{ filename = "OT_B083_FEA.bmp"; } TOFU36;
bitmap{ filename = "OT_B107_ANG.bmp"; } TOFU37;
bitmap{ filename = "OT_B107_FEA.bmp"; } TOFU38;
bitmap{ filename = "OT_B118_ANG.bmp"; } TOFU39;
bitmap{ filename = "OT_B118_FEA.bmp"; } TOFU40;
bitmap{ filename = "OT_B027_FEA.bmp"; } TOFU41;
bitmap{ filename = "OT_B027_NEU.bmp"; } TOFU42;
bitmap{ filename = "OT_B065_FEA.bmp"; } TOFU43;
bitmap{ filename = "OT_B065_NEU.bmp"; } TOFU44;
bitmap{ filename = "OT_B146_FEA.bmp"; } TOFU45;
bitmap{ filename = "OT_B146_NEU.bmp"; } TOFU46;
bitmap{ filename = "OT_B151_FEA.bmp"; } TOFU47;
bitmap{ filename = "OT_B151_NEU.bmp"; } TOFU48;
	
# ------ Young Set A ------ #
# [faces,scrambled][n,a,f][model#]
# [1,2][1,2,3][#]
bitmap{ filename = "YF_A025_ANG.bmp"; } wA1; 
bitmap{ filename = "YF_A025_NEU.bmp"; } wN1; 
bitmap{ filename = "YF_A049_ANG.bmp"; } wA2; 
bitmap{ filename = "YF_A049_NEU.bmp"; } wN2; 
bitmap{ filename = "YF_A066_ANG.bmp"; } wA3; 
bitmap{ filename = "YF_A066_NEU.bmp"; } wN3; 
bitmap{ filename = "YF_A114_ANG.bmp"; } wA4; 
bitmap{ filename = "YF_A114_NEU.bmp"; } wN4; 
bitmap{ filename = "YF_A016_ANG.bmp"; } wA5; 
bitmap{ filename = "YF_A016_FEA.bmp"; } wF5; 
bitmap{ filename = "YF_A031_ANG.bmp"; } wA6; 
bitmap{ filename = "YF_A031_FEA.bmp"; } wF6; 
bitmap{ filename = "YF_A037_ANG.bmp"; } wA7; 
bitmap{ filename = "YF_A037_FEA.bmp"; } wF7; 
bitmap{ filename = "YF_A089_ANG.bmp"; } wA8; 
bitmap{ filename = "YF_A089_FEA.bmp"; } wF8; 
bitmap{ filename = "YF_A013_FEA.bmp"; } wF9;                         
bitmap{ filename = "YF_A013_NEU.bmp"; } wN9; 
bitmap{ filename = "YF_A062_FEA.bmp"; } wF10; 
bitmap{ filename = "YF_A062_NEU.bmp"; } wN10; 
bitmap{ filename = "YF_A081_FEA.bmp"; } wF11; 
bitmap{ filename = "YF_A081_NEU.bmp"; } wN11; 
bitmap{ filename = "YF_A105_FEA.bmp"; } wF12; 
bitmap{ filename = "YF_A105_NEU.bmp"; } wN12; 
# Scrambled
bitmap{ filename = "YT_A025_ANG.bmp"; } TOFU49;
bitmap{ filename = "YT_A025_NEU.bmp"; } TOFU50;
bitmap{ filename = "YT_A049_ANG.bmp"; } TOFU51;
bitmap{ filename = "YT_A049_NEU.bmp"; } TOFU52;
bitmap{ filename = "YT_A066_ANG.bmp"; } TOFU53;
bitmap{ filename = "YT_A066_NEU.bmp"; } TOFU54;
bitmap{ filename = "YT_A114_ANG.bmp"; } TOFU55;
bitmap{ filename = "YT_A114_NEU.bmp"; } TOFU56;
bitmap{ filename = "YT_A016_ANG.bmp"; } TOFU57;
bitmap{ filename = "YT_A016_FEA.bmp"; } TOFU58;
bitmap{ filename = "YT_A031_ANG.bmp"; } TOFU59;
bitmap{ filename = "YT_A031_FEA.bmp"; } TOFU60;
bitmap{ filename = "YT_A037_ANG.bmp"; } TOFU61;
bitmap{ filename = "YT_A037_FEA.bmp"; } TOFU62;
bitmap{ filename = "YT_A089_ANG.bmp"; } TOFU63;
bitmap{ filename = "YT_A089_FEA.bmp"; } TOFU64;
bitmap{ filename = "YT_A013_FEA.bmp"; } TOFU65;
bitmap{ filename = "YT_A013_NEU.bmp"; } TOFU66;
bitmap{ filename = "YT_A062_FEA.bmp"; } TOFU67;
bitmap{ filename = "YT_A062_NEU.bmp"; } TOFU68;
bitmap{ filename = "YT_A081_FEA.bmp"; } TOFU69;
bitmap{ filename = "YT_A081_NEU.bmp"; } TOFU70;
bitmap{ filename = "YT_A105_FEA.bmp"; } TOFU71;
bitmap{ filename = "YT_A105_NEU.bmp"; } TOFU72;
	
# ------ Young Set B ------ #
bitmap{ filename = "YF_B025_ANG.bmp"; } wA13;
bitmap{ filename = "YF_B025_NEU.bmp"; } wN13;
bitmap{ filename = "YF_B049_ANG.bmp"; } wA14;
bitmap{ filename = "YF_B049_NEU.bmp"; } wN14;
bitmap{ filename = "YF_B066_ANG.bmp"; } wA15;
bitmap{ filename = "YF_B066_NEU.bmp"; } wN15;
bitmap{ filename = "YF_B114_ANG.bmp"; } wA16;
bitmap{ filename = "YF_B114_NEU.bmp"; } wN16;
bitmap{ filename = "YF_B016_ANG.bmp"; } wA17;
bitmap{ filename = "YF_B016_FEA.bmp"; } wF17;
bitmap{ filename = "YF_B031_ANG.bmp"; } wA18;
bitmap{ filename = "YF_B031_FEA.bmp"; } wF18;
bitmap{ filename = "YF_B037_ANG.bmp"; } wA19;
bitmap{ filename = "YF_B037_FEA.bmp"; } wF19;
bitmap{ filename = "YF_B089_ANG.bmp"; } wA20;
bitmap{ filename = "YF_B089_FEA.bmp"; } wF20;
bitmap{ filename = "YF_B013_FEA.bmp"; } wF21;
bitmap{ filename = "YF_B013_NEU.bmp"; } wN21;
bitmap{ filename = "YF_B062_FEA.bmp"; } wF22;
bitmap{ filename = "YF_B062_NEU.bmp"; } wN22;
bitmap{ filename = "YF_B081_FEA.bmp"; } wF23;
bitmap{ filename = "YF_B081_NEU.bmp"; } wN23;
bitmap{ filename = "YF_B105_FEA.bmp"; } wF24;
bitmap{ filename = "YF_B105_NEU.bmp"; } wN24;
# Scrambled
bitmap{ filename = "YT_B025_ANG.bmp"; } TOFU73;
bitmap{ filename = "YT_B025_NEU.bmp"; } TOFU74;
bitmap{ filename = "YT_B049_ANG.bmp"; } TOFU75;
bitmap{ filename = "YT_B049_NEU.bmp"; } TOFU76;
bitmap{ filename = "YT_B066_ANG.bmp"; } TOFU77;
bitmap{ filename = "YT_B066_NEU.bmp"; } TOFU78;
bitmap{ filename = "YT_B114_ANG.bmp"; } TOFU79;
bitmap{ filename = "YT_B114_NEU.bmp"; } TOFU80;
bitmap{ filename = "YT_B016_ANG.bmp"; } TOFU81;
bitmap{ filename = "YT_B016_FEA.bmp"; } TOFU82;
bitmap{ filename = "YT_B031_ANG.bmp"; } TOFU83;
bitmap{ filename = "YT_B031_FEA.bmp"; } TOFU84;
bitmap{ filename = "YT_B037_ANG.bmp"; } TOFU85;
bitmap{ filename = "YT_B037_FEA.bmp"; } TOFU86;
bitmap{ filename = "YT_B089_ANG.bmp"; } TOFU87;
bitmap{ filename = "YT_B089_FEA.bmp"; } TOFU88;
bitmap{ filename = "YT_B013_FEA.bmp"; } TOFU89;
bitmap{ filename = "YT_B013_NEU.bmp"; } TOFU90;
bitmap{ filename = "YT_B062_FEA.bmp"; } TOFU91;
bitmap{ filename = "YT_B062_NEU.bmp"; } TOFU92;
bitmap{ filename = "YT_B081_FEA.bmp"; } TOFU93;
bitmap{ filename = "YT_B081_NEU.bmp"; } TOFU94;
bitmap{ filename = "YT_B105_FEA.bmp"; } TOFU95;
bitmap{ filename = "YT_B105_NEU.bmp"; } TOFU96;

# ----------------------------- PCL Program -----------------------------
begin_pcl;

include_once "lib_visual_utilities.pcl";
include_once "lib_utilities.pcl";

# --- CONSTANTS --- #

string STIM_EVENT_CODE = "Stimulus";

string PRACTICE = "Practice";
string MAIN = "Main";

string LOG_ACTIVE = "log_active";

int Young_NUM = 1;
int Old_NUM = 2;

int FACE_OR_SCRAM_IDX = 1;
int age_IDX = 2;
int EMOTION_IDX = 3;
int MODEL_IDX = 4;

int NEUTRAL = 1;
int ANGRY = 2;
int FEARFUL = 3;

int CORR_BUTTON = 201;
int INCORR_BUTTON = 202;

int PORT_CODE_PREFIX = 100;

string CHARACTER_WRAP = "Character";

# =-=-=-=-=-=-=-=-=-=-=--=-=-=-= Create Stimulus Arrays =-=-=-=-=-=-=-=-=-=-=-=-=-=-= #
array<bitmap> prac_bmps[2][2][3][6];
# ==== PRACTICE BITMAP ARRAY ==== # 	 [B/W][Face/Scramble][Emotion]["model"]
	prac_bmps[1][1][2][1] = A001;
	prac_bmps[1][1][3][1] = F001;
	prac_bmps[1][1][1][1] = N001;
	prac_bmps[1][1][2][2] = A002;
	prac_bmps[1][1][3][2] = F002;
	prac_bmps[1][1][1][2] = N002;
	prac_bmps[1][1][2][3] = A003;
	prac_bmps[1][1][3][3] = F003;
	prac_bmps[1][1][1][3] = N003;
# ------------------------------------------------------ # 
	prac_bmps[2][1][2][4] = A004;
	prac_bmps[2][1][3][4] = F004;
	prac_bmps[2][1][1][4] = N004;
	prac_bmps[2][1][2][5] = A005;
	prac_bmps[2][1][3][5] = F005;
	prac_bmps[2][1][1][5] = N005;
	prac_bmps[2][1][2][6] = A006;
	prac_bmps[2][1][3][6] = F006;
	prac_bmps[2][1][1][6] = N006;
# Scrambled ____
	prac_bmps[1][2][2][1] = SCRAMBLED1;
	prac_bmps[1][2][3][1] = SCRAMBLED2;
	prac_bmps[1][2][1][1] = SCRAMBLED3;
	prac_bmps[1][2][2][2] = SCRAMBLED4;
	prac_bmps[1][2][3][2] = SCRAMBLED5;
	prac_bmps[1][2][1][2] = SCRAMBLED6;
	prac_bmps[1][2][2][3] = SCRAMBLED7;
	prac_bmps[1][2][3][3] = SCRAMBLED8;
	prac_bmps[1][2][1][3] = SCRAMBLED9;
# ------------------------------------------------------ # 
	prac_bmps[2][2][2][4] = SCRAMBLED10;
	prac_bmps[2][2][3][4] = SCRAMBLED11;
	prac_bmps[2][2][1][4] = SCRAMBLED12;
	prac_bmps[2][2][2][5] = SCRAMBLED13;
	prac_bmps[2][2][3][5] = SCRAMBLED14;
	prac_bmps[2][2][1][5] = SCRAMBLED15;
	prac_bmps[2][2][2][6] = SCRAMBLED16;
	prac_bmps[2][2][3][6] = SCRAMBLED17;
	prac_bmps[2][2][1][6] = SCRAMBLED18;
# ------ Create Set A's & B's ----- #
array<bitmap> stim_sets[4][2][3][24];
#array<bitmap> Young_set_A[1][2][3][24];
#array<bitmap> Young_set_B[2][3][24];
#array<bitmap> Old_set_A[2][3][24];
#array<bitmap> Old_set_B[2][3][24];
# [faces,scrambled][n,a,f][model#]
# [1,2][1,2,3][#]
# ------ Old Set A ------ #
	stim_sets[3][1][2][1 ] = A1; #emo 1
	stim_sets[3][1][1][1 ] = N1;   #FID 1
	stim_sets[3][1][2][2 ] = A2; #emo 2
	stim_sets[3][1][1][2 ] = N2;   #FID 2
	stim_sets[3][1][2][3 ] = A3; #emo 3
	stim_sets[3][1][1][3 ] = N3;   #FID 3
	stim_sets[3][1][2][4 ] = A4; #emo 1
	stim_sets[3][1][1][4 ] = N4;   #FID 1
	stim_sets[3][1][2][5 ] = A5;   #FID 2
	stim_sets[3][1][3][5 ] = F5; #emo 2
	stim_sets[3][1][2][6 ] = A6;   #FID 3
	stim_sets[3][1][3][6 ] = F6; #emo 3
	stim_sets[3][1][2][7 ] = A7;   #FID 1
	stim_sets[3][1][3][7 ] = F7; #emo 1
	stim_sets[3][1][2][8 ] = A8;   #FID 2
	stim_sets[3][1][3][8 ] = F8; #emo 2
	stim_sets[3][1][3][9 ] = F9;   #FID 3
	stim_sets[3][1][1][9 ] = N9; #emo 3
	stim_sets[3][1][3][10] = F10;   #FID 1
	stim_sets[3][1][1][10] = N10; #emo 1
	stim_sets[3][1][3][11] = F11;   #FID 2
	stim_sets[3][1][1][11] = N11; #emo 2
	stim_sets[3][1][3][12] = F12;   #FID 3
	stim_sets[3][1][1][12] = N12; #emo 3
# Scrambled
	stim_sets[3][2][2][1] = TOFU1;
	stim_sets[3][2][1][1] = TOFU2; #S-yes 1 n1
	stim_sets[3][2][2][2] = TOFU3;
	stim_sets[3][2][1][2] = TOFU4; #S-yes 2 n2
	stim_sets[3][2][2][3] = TOFU5;
	stim_sets[3][2][1][3] = TOFU6; #S-yes 3 n3
	stim_sets[3][2][2][4] = TOFU7;
	stim_sets[3][2][1][4] = TOFU8; #S-yes 1 n4
	stim_sets[3][2][2][5] = TOFU9; #S-yes 2 a5
	stim_sets[3][2][3][5] = TOFU10;
	stim_sets[3][2][2][6] = TOFU11; #S-yes 3 a6
	stim_sets[3][2][3][6] = TOFU12;
	stim_sets[3][2][2][7] = TOFU13; #S-yes 1 a7
	stim_sets[3][2][3][7] = TOFU14;
	stim_sets[3][2][2][8] = TOFU15; #S-yes 2 a8
	stim_sets[3][2][3][8] = TOFU16;
	stim_sets[3][2][3][9] = TOFU17; #S-yes 3 f9
	stim_sets[3][2][1][9] = TOFU18;
	stim_sets[3][2][3][10] = TOFU19; #S-yes 1 f10
	stim_sets[3][2][1][10] = TOFU20;
	stim_sets[3][2][3][11] = TOFU21; #S-yes 2 f11
	stim_sets[3][2][1][11] = TOFU22;
	stim_sets[3][2][3][12] = TOFU23; #S-yes 3 f12
	stim_sets[3][2][1][12] = TOFU24;
# ------ Old Set B ------ #
	stim_sets[4][1][2][13] = A13;   #FID 1
	stim_sets[4][1][1][13] = N13; #emo 1
	stim_sets[4][1][2][14] = A14;   #FID 2
	stim_sets[4][1][1][14] = N14; #emo 2
	stim_sets[4][1][2][15] = A15;   #FID 3
	stim_sets[4][1][1][15] = N15; #emo 3
	stim_sets[4][1][2][16] = A16;   #FID 1
	stim_sets[4][1][1][16] = N16; #emo 1
	stim_sets[4][1][2][17] = A17; #emo 2
	stim_sets[4][1][3][17] = F17;   #FID 2
	stim_sets[4][1][2][18] = A18; #emo 3
	stim_sets[4][1][3][18] = F18;   #FID 3
	stim_sets[4][1][2][19] = A19; #emo 1
	stim_sets[4][1][3][19] = F19;   #FID 1
	stim_sets[4][1][2][20] = A20; #emo 2
	stim_sets[4][1][3][20] = F20;   #FID 2
	stim_sets[4][1][3][21] = F21; #emo 3
	stim_sets[4][1][1][21] = N21;   #FID 3
	stim_sets[4][1][3][22] = F22; #emo 1
	stim_sets[4][1][1][22] = N22;   #FID 1
	stim_sets[4][1][3][23] = F23; #emo 2
	stim_sets[4][1][1][23] = N23;   #FID 2
	stim_sets[4][1][3][24] = F24; #emo 3
	stim_sets[4][1][1][24] = N24;   #FID 3
# Scrambled
	stim_sets[4][2][2][13] = TOFU25;
	stim_sets[4][2][1][13] = TOFU26; #S-yes 1
	stim_sets[4][2][2][14] = TOFU27;
	stim_sets[4][2][1][14] = TOFU28; #S-yes 2
	stim_sets[4][2][2][15] = TOFU29;
	stim_sets[4][2][1][15] = TOFU30; #S-yes 3
	stim_sets[4][2][2][16] = TOFU31;
	stim_sets[4][2][1][16] = TOFU32; #S-yes 1
	stim_sets[4][2][2][17] = TOFU33; #S-yes 2
	stim_sets[4][2][3][17] = TOFU34;
	stim_sets[4][2][2][18] = TOFU35; #S-yes 3
	stim_sets[4][2][3][18] = TOFU36;
	stim_sets[4][2][2][19] = TOFU37; #S-yes 1
	stim_sets[4][2][3][19] = TOFU38;
	stim_sets[4][2][2][20] = TOFU39; #S-yes 2
	stim_sets[4][2][3][20] = TOFU40;
	stim_sets[4][2][3][21] = TOFU41; #S-yes 3
	stim_sets[4][2][1][21] = TOFU42;
	stim_sets[4][2][3][22] = TOFU43; #S-yes 1
	stim_sets[4][2][1][22] = TOFU44;
	stim_sets[4][2][3][23] = TOFU45; #S-yes 2
	stim_sets[4][2][1][23] = TOFU46;
	stim_sets[4][2][3][24] = TOFU47; #S-yes 3
	stim_sets[4][2][1][24] = TOFU48;
#------------- Young SET A =================== #
	stim_sets[1][1][2][1] = wA1; #emo1
	stim_sets[1][1][1][1] = wN1;   #FID 1
	stim_sets[1][1][2][2] = wA2; #emo2
	stim_sets[1][1][1][2] = wN2;   #FID 2
	stim_sets[1][1][2][3] = wA3; #emo3
	stim_sets[1][1][1][3] = wN3;   #FID 3
	stim_sets[1][1][2][4] = wA4; #emo1
	stim_sets[1][1][1][4] = wN4;   #FID 1
	stim_sets[1][1][2][5] = wA5;   #FID 2
	stim_sets[1][1][3][5] = wF5; #emo2
	stim_sets[1][1][2][6] = wA6;   #FID 3
	stim_sets[1][1][3][6] = wF6; #emo3
	stim_sets[1][1][2][7] = wA7;   #FID 1
	stim_sets[1][1][3][7] = wF7; #emo1
	stim_sets[1][1][2][8] = wA8;   #FID 2
	stim_sets[1][1][3][8] = wF8; #emo2
	stim_sets[1][1][3][9] = wF9;   #FID 3
	stim_sets[1][1][1][9] = wN9; #emo3
	stim_sets[1][1][3][10] = wF10;   #FID 1
	stim_sets[1][1][1][10] = wN10; #emo1
	stim_sets[1][1][3][11] = wF11;   #FID 2
	stim_sets[1][1][1][11] = wN11; #emo2
	stim_sets[1][1][3][12] = wF12;   #FID 3
	stim_sets[1][1][1][12] = wN12; #emo3
# Scrambled
	stim_sets[1][2][2][1] = TOFU49;
	stim_sets[1][2][1][1] = TOFU50; #S-yes 1
	stim_sets[1][2][2][2] = TOFU51;
	stim_sets[1][2][1][2] = TOFU52; #S-yes 2
	stim_sets[1][2][2][3] = TOFU53;
	stim_sets[1][2][1][3] = TOFU54; #S-yes 3
	stim_sets[1][2][2][4] = TOFU55;
	stim_sets[1][2][1][4] = TOFU56; #S-yes 1
	stim_sets[1][2][2][5] = TOFU57; #S-yes 2
	stim_sets[1][2][3][5] = TOFU58;
	stim_sets[1][2][2][6] = TOFU59; #S-yes 3
	stim_sets[1][2][3][6] = TOFU60;
	stim_sets[1][2][2][7] = TOFU61; #S-yes 1
	stim_sets[1][2][3][7] = TOFU62;
	stim_sets[1][2][2][8] = TOFU63; #S-yes 2
	stim_sets[1][2][3][8] = TOFU64;
	stim_sets[1][2][3][9] = TOFU65; #S-yes 3
	stim_sets[1][2][1][9] = TOFU66;
	stim_sets[1][2][3][10] = TOFU67; #S-yes 1
	stim_sets[1][2][1][10] = TOFU68;
	stim_sets[1][2][3][11] = TOFU69; #S-yes 2
	stim_sets[1][2][1][11] = TOFU70;
	stim_sets[1][2][3][12] = TOFU71; #S-yes 3
	stim_sets[1][2][1][12] = TOFU72;
# ------ Young Set B ------ #
	stim_sets[2][1][2][13] = wA13;   #FID 1
	stim_sets[2][1][1][13] = wN13; #emo 1
	stim_sets[2][1][2][14] = wA14;   #FID 2
	stim_sets[2][1][1][14] = wN14; #emo 2
	stim_sets[2][1][2][15] = wA15;   #FID 3
	stim_sets[2][1][1][15] = wN15; #emo 3
	stim_sets[2][1][2][16] = wA16;   #FID 1
	stim_sets[2][1][1][16] = wN16; #emo 1
	stim_sets[2][1][2][17] = wA17; #emo 2
	stim_sets[2][1][3][17] = wF17;   #FID 2
	stim_sets[2][1][2][18] = wA18; #emo 3
	stim_sets[2][1][3][18] = wF18;   #FID 3
	stim_sets[2][1][2][19] = wA19; #emo 1
	stim_sets[2][1][3][19] = wF19;   #FID 1
	stim_sets[2][1][2][20] = wA20; #emo 2
	stim_sets[2][1][3][20] = wF20;   #FID 2
	stim_sets[2][1][3][21] = wF21; #emo 3
	stim_sets[2][1][1][21] = wN21;   #FID 3
	stim_sets[2][1][3][22] = wF22; #emo 1
	stim_sets[2][1][1][22] = wN22;   #FID 1
	stim_sets[2][1][3][23] = wF23; #emo 2
	stim_sets[2][1][1][23] = wN23;   #FID 2
	stim_sets[2][1][3][24] = wF24; #emo 3
	stim_sets[2][1][1][24] = wN24;   #FID 3
# Scrambled
	stim_sets[2][2][2][13] = TOFU73;
	stim_sets[2][2][1][13] = TOFU74; #S-yes 1
	stim_sets[2][2][2][14] = TOFU75;
	stim_sets[2][2][1][14] = TOFU76; #S-yes 2
	stim_sets[2][2][2][15] = TOFU77;
	stim_sets[2][2][1][15] = TOFU78; #S-yes 3
	stim_sets[2][2][2][16] = TOFU79;
	stim_sets[2][2][1][16] = TOFU80; #S-yes 1
	stim_sets[2][2][2][17] = TOFU81; #S-yes 2
	stim_sets[2][2][3][17] = TOFU82;
	stim_sets[2][2][2][18] = TOFU83; #S-yes 3
	stim_sets[2][2][3][18] = TOFU84;
	stim_sets[2][2][2][19] = TOFU85; #S-yes 1
	stim_sets[2][2][3][19] = TOFU86;
	stim_sets[2][2][2][20] = TOFU87; #S-yes 2
	stim_sets[2][2][3][20] = TOFU88;
	stim_sets[2][2][3][21] = TOFU89; #S-yes 3
	stim_sets[2][2][1][21] = TOFU90;
	stim_sets[2][2][3][22] = TOFU91; #S-yes 1
	stim_sets[2][2][1][22] = TOFU92;
	stim_sets[2][2][3][23] = TOFU93; #S-yes 2
	stim_sets[2][2][1][23] = TOFU94;
	stim_sets[2][2][3][24] = TOFU95; #S-yes 3
	stim_sets[2][2][1][24] = TOFU96;
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< #
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> #
array<int> all_info[4][3][76][5];
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~ FIRST ORDER ~~~~~~~~~~~~~~~~~
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
						#{ type, age, emotion, modNUM, repeat }
all_info[1][1][1]	 = { 1, 1, 3, 10, 0 };
all_info[1][1][2]	 = { 1, 1, 1, 4, 	0 };
all_info[1][1][3]	 = { 1, 1, 3, 11, 0 };
all_info[1][1][4]	 = { 1, 1, 2, 6, 	0 };
all_info[1][1][5]	 = { 1, 1, 1, 1, 	0 };
all_info[1][1][6]	 = { 1, 1, 2, 1, 	2 }; ##FID ANG 1
all_info[1][1][7]	 = { 2, 1, 3, 11, 0 };
all_info[1][1][8]	 = { 1, 1, 2, 7, 	0 };
all_info[1][1][9]	 = { 1, 1, 1, 2, 	0 };
all_info[1][1][10] = { 1, 1, 3, 9, 	0 };
all_info[1][1][11] = { 2, 1, 1, 12, 	0 };
all_info[1][1][12] = { 1, 1, 1, 11, 0 };
all_info[1][1][13] = { 1, 1, 3, 12, 0 };
all_info[1][1][14] = { 1, 1, 3, 8, 1 }; ##EMO FEAR 8
all_info[1][1][15] = { 1, 1, 2, 2, 0 };
all_info[1][1][16] = { 1, 1, 1, 3, 0 };
all_info[1][1][17] = { 1, 1, 1, 4, 1 }; ##EMO NEUT 4
all_info[1][1][18] = { 2, 1, 1, 3, 0 };
all_info[1][1][19] = { 1, 1, 3, 7, 0 };
all_info[1][1][20] = { 1, 1, 2, 3, 0 };
all_info[1][1][21] = { 1, 1, 3, 12, 0 };
all_info[1][1][22] = { 1, 1, 2, 5, 0 };
all_info[1][1][23] = { 1, 1, 1, 1, 0 };
all_info[1][1][24] = { 1, 1, 3, 9, 0 };
all_info[1][1][25] = { 1, 1, 2, 8, 0 };
all_info[1][1][26] = { 1, 1, 1, 3, 0 };
all_info[1][1][27] = { 1, 1, 1, 10, 1}; ##EMO NEUT 10
all_info[1][1][28] = { 1, 1, 2, 2, 0 };
all_info[1][1][29] = { 2, 1, 1, 4, 0 };
all_info[1][1][30] = { 1, 1, 3, 9, 0 };
all_info[1][1][31] = { 1, 1, 1, 9, 2 }; ##FID NEUT 9
all_info[1][1][32] = { 2, 1, 3, 10, 0 };
all_info[1][1][33] = { 1, 1, 1, 10, 0 };
all_info[1][1][34] = { 2, 1, 3, 6, 0 };
all_info[1][1][35] = { 1, 1, 2, 6, 0 };
all_info[1][1][36] = { 1, 1, 3, 6, 2 }; ##FID FEAR 6
all_info[1][1][37] = { 1, 1, 1, 11, 0 };
all_info[1][1][38] = { 1, 1, 2, 4, 0 };
all_info[1][1][39] = { 1, 1, 3, 7, 0 };
all_info[1][1][40] = { 1, 1, 2, 3, 0 };
all_info[1][1][41] = { 2, 1, 2, 7, 0 };
all_info[1][1][42] = { 1, 1, 3, 5, 0 };
all_info[1][1][43] = { 2, 1, 3, 8, 0 };
all_info[1][1][44] = { 2, 1, 3, 8, 3 }; ##SCRAM FEAR 8
all_info[1][1][45] = { 1, 1, 1, 12, 0 };
all_info[1][1][46] = { 2, 1, 1, 9, 0 };
all_info[1][1][47] = { 1, 1, 2, 8, 0 };
all_info[1][1][48] = { 2, 1, 3, 10, 0 };
all_info[1][1][49] = { 1, 1, 1, 10, 0 };
all_info[1][1][50] = { 1, 1, 3, 6, 0 };
all_info[1][1][51] = { 1, 1, 2, 7, 0 };
all_info[1][1][52] = { 1, 1, 3, 7, 2 }; ##FID FEAR 7
all_info[1][1][53] = { 1, 1, 1, 4, 0 };
all_info[1][1][54] = { 1, 1, 3, 11, 0 };
all_info[1][1][55] = { 2, 1, 3, 12, 0 };
all_info[1][1][56] = { 1, 1, 1, 12, 0 };
all_info[1][1][57] = { 2, 1, 3, 7, 0 };
all_info[1][1][58] = { 2, 1, 3, 7, 3 }; ##SCRAM FEAR 7
all_info[1][1][59] = { 1, 1, 3, 8, 0 };
all_info[1][1][60] = { 2, 1, 2, 1, 0 };
all_info[1][1][61] = { 1, 1, 2, 1, 0 };
all_info[1][1][62] = { 1, 1, 1, 9, 0 };
all_info[1][1][63] = { 1, 1, 1, 11, 1 }; ##EMO NEUT 11
all_info[1][1][64] = { 1, 1, 3, 6, 0 };
all_info[1][1][65] = { 1, 1, 1, 2, 0 };
all_info[1][1][66] = { 1, 1, 3, 10, 0 };
all_info[1][1][67] = { 1, 1, 2, 4, 0 };
all_info[1][1][68] = { 2, 1, 1, 2, 0 };
all_info[1][1][69] = { 2, 1, 1, 2, 3 }; ##SCRAM NEUT 2
all_info[1][1][70] = { 1, 1, 2, 3, 0 };
all_info[1][1][71] = { 2, 1, 2, 3, 0 };
all_info[1][1][72] = { 2, 1, 2, 3, 3 }; ##SCRAM ANG 3
all_info[1][1][73] = { 1, 1, 2, 5, 0 };
all_info[1][1][74] = { 2, 1, 2, 4, 0 };
all_info[1][1][75] = { 1, 1, 2, 1, 0 };
all_info[1][1][76] = { 1, 1, 3, 5, 0 };
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~ SECOND ORDER ~~~~~~~~~~~~~~~~~
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
all_info[1][2][1] = { 2, 1, 1, 1, 0 };
all_info[1][2][2] = { 2, 1, 1, 1, 3 }; ##SCRAM NEUT 1
all_info[1][2][3] = { 1, 1, 3, 5, 0 };
all_info[1][2][4] = { 1, 1, 1, 9, 0 };
all_info[1][2][5] = { 1, 1, 3, 6, 0 };
all_info[1][2][6] = { 2, 1, 2, 2, 0 };
all_info[1][2][7] = { 2, 1, 2, 2, 3 }; ##SCRAM ANG 2
all_info[1][2][8] = { 1, 1, 3, 11, 0 };
all_info[1][2][9] = { 1, 1, 1, 3, 0 };
all_info[1][2][10] = { 1, 1, 3, 10, 0 };
all_info[1][2][11] = { 2, 1, 2, 3, 0 };
all_info[1][2][12] = { 1, 1, 1, 12, 0 };
all_info[1][2][13] = { 1, 1, 3, 5, 0 };
all_info[1][2][14] = { 1, 1, 1, 1, 0 };
all_info[1][2][15] = { 1, 1, 2, 6, 0 };
all_info[1][2][16] = { 2, 1, 2, 4, 0 };
all_info[1][2][17] = { 1, 1, 1, 2, 0 };
all_info[1][2][18] = { 1, 1, 2, 2, 2 }; ##FID ANG 2
all_info[1][2][19] = { 1, 1, 1, 12, 0 };
all_info[1][2][20] = { 1, 1, 2, 7, 0 };
all_info[1][2][21] = { 1, 1, 2, 5, 1 }; ##EMO ANG 5
all_info[1][2][22] = { 1, 1, 1, 10, 0 };
all_info[1][2][23] = { 1, 1, 2, 3, 0 };
all_info[1][2][24] = { 2, 1, 2, 5, 0 };
all_info[1][2][25] = { 1, 1, 1, 1, 0 };
all_info[1][2][26] = { 1, 1, 3, 11, 0 };
all_info[1][2][27] = { 1, 1, 2, 6, 0 };
all_info[1][2][28] = { 1, 1, 3, 8, 0 };
all_info[1][2][29] = { 1, 1, 1, 10, 0 };
all_info[1][2][30] = { 1, 1, 2, 5, 0 };
all_info[1][2][31] = { 1, 1, 1, 4, 0 };
all_info[1][2][32] = { 1, 1, 2, 4, 2 }; ##FID ANG 4
all_info[1][2][33] = { 2, 1, 2, 6, 0 };
all_info[1][2][34] = { 1, 1, 3, 12, 0 };
all_info[1][2][35] = { 2, 1, 2, 7, 0 };
all_info[1][2][36] = { 2, 1, 2, 7, 3 }; ##SCRAM ANG 7
all_info[1][2][37] = { 1, 1, 2, 8, 0 };
all_info[1][2][38] = { 1, 1, 1, 11, 0 };
all_info[1][2][39] = { 1, 1, 1, 9, 1 }; ##EMO NEUT 9
all_info[1][2][40] = { 1, 1, 3, 8, 0 };
all_info[1][2][41] = { 1, 1, 2, 7, 0 };
all_info[1][2][42] = { 2, 1, 2, 8, 0 };
all_info[1][2][43] = { 1, 1, 3, 11, 0 };
all_info[1][2][44] = { 2, 1, 1, 10, 0 };
all_info[1][2][45] = { 1, 1, 2, 3, 0 };
all_info[1][2][46] = { 1, 1, 1, 3, 2 }; ##FID NEUT 3
all_info[1][2][47] = { 2, 1, 1, 10, 0 };
all_info[1][2][48] = { 1, 1, 3, 12, 0 };
all_info[1][2][49] = { 1, 1, 2, 4, 0 };
all_info[1][2][50] = { 1, 1, 3, 10, 0 };
all_info[1][2][51] = { 1, 1, 1, 9, 0 };
all_info[1][2][52] = { 1, 1, 2, 4, 0 };
all_info[1][2][53] = { 2, 1, 3, 6, 0 };
all_info[1][2][54] = { 1, 1, 3, 9, 0 };
all_info[1][2][55] = { 1, 1, 1, 11, 0 };
all_info[1][2][56] = { 1, 1, 2, 6, 0 };
all_info[1][2][57] = { 1, 1, 2, 2, 1 }; ##EMO ANG 2
all_info[1][2][58] = { 2, 1, 1, 12, 0 };
all_info[1][2][59] = { 1, 1, 3, 5, 0 };
all_info[1][2][60] = { 1, 1, 2, 8, 0 };
all_info[1][2][61] = { 1, 1, 3, 8, 2 }; ## FID FEAR 8
all_info[1][2][62] = { 1, 1, 1, 4, 0 };
all_info[1][2][63] = { 2, 1, 1, 10, 0 };
all_info[1][2][64] = { 1, 1, 2, 1, 0 };
all_info[1][2][65] = { 1, 1, 3, 9, 0 };
all_info[1][2][66] = { 1, 1, 3, 12, 1 }; ##EMO FEAR 12
all_info[1][2][67] = { 1, 1, 1, 1, 0 };
all_info[1][2][68] = { 1, 1, 2, 7, 0 };
all_info[1][2][69] = { 2, 1, 3, 9, 0 };
all_info[1][2][70] = { 1, 1, 2, 5, 0 };
all_info[1][2][71] = { 2, 1, 1, 11, 0 };
all_info[1][2][72] = { 2, 1, 1, 11, 3 }; ##SCRAM NEUT 11
all_info[1][2][73] = { 1, 1, 2, 2, 0 };
all_info[1][2][74] = { 2, 1, 3, 12, 0 };
all_info[1][2][75] = { 1, 1, 3, 7, 0 };
all_info[1][2][76] = { 1, 1, 1, 2, 0 };
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~ THIRD ORDER ~~~~~~~~~~~~~~~~~
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
all_info[1][3][1] = { 1, 1, 2, 7, 0 };
all_info[1][3][2] = { 2, 1, 2, 1, 0 };
all_info[1][3][3] = { 2, 1, 2, 1, 3 }; ##SCRAM ANG 1
all_info[1][3][4] = { 1, 1, 3, 10, 0 };
all_info[1][3][5] = { 1, 1, 2, 2, 0 };
all_info[1][3][6] = { 2, 1, 1, 2, 0 };
all_info[1][3][7] = { 1, 1, 1, 12, 0 };
all_info[1][3][8] = { 1, 1, 1, 2, 1 }; ##EMO NEUT 2
all_info[1][3][9] = { 1, 1, 3, 8, 0 };
all_info[1][3][10] = { 2, 1, 1, 3, 0 };
all_info[1][3][11] = { 1, 1, 3, 8, 0 };
all_info[1][3][12] = { 1, 1, 1, 9, 0 };
all_info[1][3][13] = { 1, 1, 3, 5, 0 };
all_info[1][3][14] = { 1, 1, 3, 7, 1 }; ##EMO FEAR 7
all_info[1][3][15] = { 2, 1, 1, 4, 0 };
all_info[1][3][16] = { 1, 1, 3, 5, 0 };
all_info[1][3][17] = { 1, 1, 1, 4, 0 };
all_info[1][3][18] = { 2, 1, 2, 4, 0 }; 
all_info[1][3][19] = { 1, 1, 3, 6, 0 };
all_info[1][3][20] = { 2, 1, 3, 5, 0 };
all_info[1][3][21] = { 2, 1, 3, 5, 3 }; ##SCRAM FEAR 5
all_info[1][3][22] = { 1, 1, 2, 3, 0 };
all_info[1][3][23] = { 2, 1, 3, 6, 0 };
all_info[1][3][24] = { 1, 1, 1, 11, 0 };
all_info[1][3][25] = { 1, 1, 2, 4, 0 };
all_info[1][3][26] = { 1, 1, 3, 7, 0 };
all_info[1][3][27] = { 1, 1, 3, 10, 1 }; ##EMO FEAR 10
all_info[1][3][28] = { 1, 1, 2, 2, 0 };
all_info[1][3][29] = { 1, 1, 3, 11, 0 };
all_info[1][3][30] = { 1, 1, 1, 11, 2 }; # FID NEUT 11
all_info[1][3][31] = { 1, 1, 2, 3, 0 };
all_info[1][3][32] = { 1, 1, 1, 1, 0 };
all_info[1][3][33] = { 1, 1, 3, 6, 0 };
all_info[1][3][34] = { 1, 1, 2, 1, 0 };
all_info[1][3][35] = { 1, 1, 1, 12, 0 };
all_info[1][3][36] = { 2, 1, 3, 8, 0 };
all_info[1][3][37] = { 1, 1, 3, 12, 0 };
all_info[1][3][38] = { 1, 1, 1, 2, 0 };
all_info[1][3][39] = { 1, 1, 3, 9, 0 };
all_info[1][3][40] = { 1, 1, 2, 1, 0 };
all_info[1][3][41] = { 1, 1, 1, 12, 0 };
all_info[1][3][42] = { 1, 1, 3, 12, 2 }; ## FID FEAR 12
all_info[1][3][43] = { 2, 1, 1, 9, 0 };
all_info[1][3][44] = { 2, 1, 1, 9, 3 }; ##SCRAM NEUT 9
all_info[1][3][45] = { 1, 1, 2, 8, 0 };
all_info[1][3][46] = { 1, 1, 1, 9, 0 };
all_info[1][3][47] = { 1, 1, 2, 7, 0 };
all_info[1][3][48] = { 2, 1, 3, 9, 0 };
all_info[1][3][49] = { 1, 1, 1, 3, 0 };
all_info[1][3][50] = { 1, 1, 3, 11, 0 };
all_info[1][3][51] = { 1, 1, 2, 8, 0 };
all_info[1][3][52] = { 1, 1, 3, 7, 0 }; 
all_info[1][3][53] = { 1, 1, 1, 3, 0 };
all_info[1][3][54] = { 2, 1, 3, 11, 0 };
all_info[1][3][55] = { 2, 1, 3, 11, 3 }; ##SCRAM FEAR 11
all_info[1][3][56] = { 1, 1, 1, 10, 0 };
all_info[1][3][57] = { 1, 1, 3, 9, 0 };
all_info[1][3][58] = { 2, 1, 1, 12, 0 };
all_info[1][3][59] = { 1, 1, 1, 4, 0 };
all_info[1][3][60] = { 1, 1, 1, 1, 1 }; ##EMO NEUT 1
all_info[1][3][61] = { 1, 1, 2, 5, 0 }; 
all_info[1][3][62] = { 1, 1, 3, 6, 0 };
all_info[1][3][63] = { 1, 1, 2, 1, 0 };
all_info[1][3][64] = { 2, 1, 3, 5, 0 };
all_info[1][3][65] = { 1, 1, 2, 5, 2 }; ## FID ANG 5
all_info[1][3][66] = { 1, 1, 2, 6, 0 };
all_info[1][3][67] = { 1, 1, 1, 2, 0 };
all_info[1][3][68] = { 1, 1, 2, 6, 0 };
all_info[1][3][69] = { 2, 1, 2, 6, 0 };
all_info[1][3][70] = { 1, 1, 1, 3, 0 };
all_info[1][3][71] = { 1, 1, 2, 4, 0 }; 
all_info[1][3][72] = { 2, 1, 2, 5, 0 };
all_info[1][3][73] = { 1, 1, 3, 10, 0 };
all_info[1][3][74] = { 1, 1, 1, 10, 2 }; ## FID NEUT 10
all_info[1][3][75] = { 1, 1, 2, 8, 0 };
all_info[1][3][76] = { 2, 1, 2, 8, 0 };
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~ FOURTH ORDER ~~~~~~~~~~~~~~~~~
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
all_info[2][1][1] = { 1, 1, 1, 3, 0 };
all_info[2][1][2] = { 2, 1, 1, 1, 0 }; 
all_info[2][1][3] = { 1, 1, 1, 12, 0 };
all_info[2][1][4] = { 1, 1, 2, 2, 0 };
all_info[2][1][5] = { 2, 1, 2, 2, 0 };
all_info[2][1][6] = { 1, 1, 3, 7, 0 };
all_info[2][1][7] = { 1, 1, 1, 3, 0 };
all_info[2][1][8] = { 2, 1, 1, 3, 0 };
all_info[2][1][9] = { 2, 1, 1, 3, 3 }; ##SCRAM NEUT 3
all_info[2][1][10] = { 1, 1, 1, 11, 0 };
all_info[2][1][11] = { 2, 1, 1, 4, 0 };
all_info[2][1][12] = { 2, 1, 1, 4, 3 }; ##SCRAM NEUT 4
all_info[2][1][13] = { 1, 1, 3, 7, 0 };
all_info[2][1][14] = { 2, 1, 3, 5, 0 };
all_info[2][1][15] = { 1, 1, 1, 10, 0 };
all_info[2][1][16] = { 1, 1, 3, 10, 2 }; ##FID FEAR 10
all_info[2][1][17] = { 2, 1, 2, 2, 0 };
all_info[2][1][18] = { 1, 1, 1, 10, 0 };
all_info[2][1][19] = { 1, 1, 3, 8, 0 };
all_info[2][1][20] = { 1, 1, 3, 5, 1 }; ##EMO FEAR 5
all_info[2][1][21] = { 1, 1, 2, 1, 0 };
all_info[2][1][22] = { 1, 1, 1, 10, 0 };
all_info[2][1][23] = { 1, 1, 2, 6, 0 };
all_info[2][1][24] = { 2, 1, 2, 7, 0 };
all_info[2][1][25] = { 1, 1, 3, 12, 0 };
all_info[2][1][26] = { 1, 1, 2, 4, 0 };
all_info[2][1][27] = { 1, 1, 2, 3, 1 }; ##EMO NEUT 3
all_info[2][1][28] = { 1, 1, 1, 2, 0 };
all_info[2][1][29] = { 1, 1, 2, 3, 0 };
all_info[2][1][30] = { 1, 1, 3, 5, 0 };
all_info[2][1][31] = { 1, 1, 3, 6, 1 }; ##EMO FEAR 6 
all_info[2][1][32] = { 1, 1, 2, 4, 0 };
all_info[2][1][33] = { 2, 1, 3, 6, 0 };
all_info[2][1][34] = { 2, 1, 3, 6, 3 }; ##SCRAM FEAR 6
all_info[2][1][35] = { 1, 1, 1, 1, 0 };
all_info[2][1][36] = { 2, 1, 1, 9, 0 };
all_info[2][1][37] = { 1, 1, 3, 10, 0 };
all_info[2][1][38] = { 1, 1, 1, 2, 0 };
all_info[2][1][39] = { 1, 1, 2, 6, 0 };
all_info[2][1][40] = { 1, 1, 3, 7, 0 };
all_info[2][1][41] = { 1, 1, 1, 9, 0 };
all_info[2][1][42] = { 1, 1, 2, 5, 0 };
all_info[2][1][43] = { 2, 1, 3, 10, 0 };
all_info[2][1][44] = { 1, 1, 1, 12, 0 };
all_info[2][1][45] = { 1, 1, 2, 3, 0 };
all_info[2][1][46] = { 1, 1, 1, 11, 0 };
all_info[2][1][47] = { 2, 1, 2, 8, 0 };
all_info[2][1][48] = { 2, 1, 2, 8, 3 }; ##SCRAM ANG 8
all_info[2][1][49] = { 1, 1, 3, 11, 0 };
all_info[2][1][50] = { 1, 1, 1, 4, 0 };
all_info[2][1][51] = { 1, 1, 3, 11, 0 };
all_info[2][1][52] = { 1, 1, 2, 4, 0 };
all_info[2][1][53] = { 1, 1, 1, 4, 2 }; # FID NEUT 4
all_info[2][1][54] = { 1, 1, 3, 5, 0 };
all_info[2][1][55] = { 1, 1, 2, 8, 0 };
all_info[2][1][56] = { 1, 1, 3, 12, 0 };
all_info[2][1][57] = { 1, 1, 1, 12, 2 }; ##FID NEUT 12 
all_info[2][1][58] = { 1, 1, 2, 7, 0 };
all_info[2][1][59] = { 1, 1, 1, 9, 0 };
all_info[2][1][60] = { 2, 1, 3, 11, 0 };
all_info[2][1][61] = { 1, 1, 2, 1, 0 };
all_info[2][1][62] = { 1, 1, 2, 7, 1 }; ##EMO ANG 7
all_info[2][1][63] = { 1, 1, 2, 8, 0 };
all_info[2][1][64] = { 1, 1, 3, 6, 0 };
all_info[2][1][65] = { 1, 1, 2, 2, 0 };
all_info[2][1][66] = { 1, 1, 3, 9, 0 };
all_info[2][1][67] = { 2, 1, 1, 2, 0 };
all_info[2][1][68] = { 1, 1, 2, 5, 0 };
all_info[2][1][69] = { 2, 1, 2, 3, 0 };
all_info[2][1][70] = { 1, 1, 3, 9, 0 };
all_info[2][1][71] = { 1, 1, 2, 1, 0 };
all_info[2][1][72] = { 1, 1, 1, 1, 2 }; ##FID NEUT 1
all_info[2][1][73] = { 1, 1, 3, 8, 0 };
all_info[2][1][74] = { 2, 1, 2, 7, 0 };
all_info[2][1][75] = { 1, 1, 1, 2, 0 };
all_info[2][1][76] = { 2, 1, 3, 8, 0 };
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~ FIFTH ORDER ~~~~~~~~~~~~~~~~~
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
all_info[2][2][1] = { 1, 1, 1, 9, 0 };
all_info[2][2][2] = { 1, 1, 2, 1, 0 };
all_info[2][2][3] = { 1, 1, 3, 6, 0 };
all_info[2][2][4] = { 1, 1, 2, 6, 2 }; ##FID ANG 6
all_info[2][2][5] = { 1, 1, 1, 4, 0 };
all_info[2][2][6] = { 2, 1, 2, 1, 0 };
all_info[2][2][7] = { 1, 1, 3, 7, 0 };
all_info[2][2][8] = { 1, 1, 1, 3, 0 };
all_info[2][2][9] = { 1, 1, 3, 11, 0 };
all_info[2][2][10] = { 1, 1, 1, 12, 0 };
all_info[2][2][11] = { 1, 1, 2, 7, 0 };
all_info[2][2][12] = { 1, 1, 3, 12, 0 };
all_info[2][2][13] = { 1, 1, 2, 3, 0 };
all_info[2][2][14] = { 1, 1, 1, 4, 0 };
all_info[2][2][15] = { 1, 1, 2, 8, 0 };
all_info[2][2][16] = { 1, 1, 1, 10, 0 };
all_info[2][2][17] = { 1, 1, 2, 2, 0 };
all_info[2][2][18] = { 1, 1, 2, 8, 1 }; ##EMO ANG 8
all_info[2][2][19] = { 2, 1, 1, 2, 0 };
all_info[2][2][20] = { 1, 1, 3, 10, 0 };
all_info[2][2][21] = { 2, 1, 2, 4, 0 };
all_info[2][2][22] = { 2, 1, 2, 4, 3 }; ##SCRAM ANG 4
all_info[2][2][23] = { 1, 1, 3, 7, 0 };
all_info[2][2][24] = { 2, 1, 1, 12, 0 };
all_info[2][2][25] = { 2, 1, 1, 12, 3 }; ##SCRAM NEUT 12
all_info[2][2][26] = { 1, 1, 1, 12, 0 };
all_info[2][2][27] = { 1, 1, 3, 10, 0 };
all_info[2][2][28] = { 2, 1, 2, 5, 0 };
all_info[2][2][29] = { 2, 1, 2, 5, 3 }; ##SCRAM ANG 5
all_info[2][2][30] = { 1, 1, 1, 1, 0 };
all_info[2][2][31] = { 2, 1, 2, 3, 0 };
all_info[2][2][32] = { 1, 1, 2, 5, 0 };
all_info[2][2][33] = { 1, 1, 3, 5, 2 }; ## FID FEAR 5
all_info[2][2][34] = { 1, 1, 1, 2, 0 };
all_info[2][2][35] = { 1, 1, 3, 12, 0 };
all_info[2][2][36] = { 2, 1, 3, 8, 0 };
all_info[2][2][37] = { 1, 1, 3, 6, 0 };
all_info[2][2][38] = { 1, 1, 1, 1, 0 };
all_info[2][2][39] = { 2, 1, 2, 7, 0 };
all_info[2][2][40] = { 1, 1, 3, 12, 0 };
all_info[2][2][41] = { 2, 1, 3, 5, 0 };
all_info[2][2][42] = { 1, 1, 1, 9, 0 };
all_info[2][2][43] = { 2, 1, 3, 6, 0 };
all_info[2][2][44] = { 1, 1, 2, 8, 0 };
all_info[2][2][45] = { 2, 1, 3, 7, 0 };
all_info[2][2][46] = { 1, 1, 1, 11, 0 };
all_info[2][2][47] = { 1, 1, 2, 6, 0 };
all_info[2][2][48] = { 1, 1, 1, 4, 0 };
all_info[2][2][49] = { 1, 1, 3, 11, 0 };
all_info[2][2][50] = { 1, 1, 1, 3, 0 };
all_info[2][2][51] = { 1, 1, 2, 3, 2 }; ##FID ANG 3
all_info[2][2][52] = { 1, 1, 3, 8, 0 };
all_info[2][2][53] = { 1, 1, 2, 5, 0 };
all_info[2][2][54] = { 2, 1, 3, 12, 0 };
all_info[2][2][55] = { 2, 1, 3, 12, 3 }; ##SCRAM FEAR 12
all_info[2][2][56] = { 1, 1, 2, 1, 0 };
all_info[2][2][57] = { 1, 1, 3, 6, 0 };
all_info[2][2][58] = { 1, 1, 1, 10, 0 };
all_info[2][2][59] = { 1, 1, 3, 8, 0 };
all_info[2][2][60] = { 1, 1, 3, 11, 1 }; ##EMO FEAR 11
all_info[2][2][61] = { 1, 1, 2, 7, 0 };
all_info[2][2][62] = { 2, 1, 3, 9, 0 };
all_info[2][2][63] = { 1, 1, 2, 2, 0 };
all_info[2][2][64] = { 1, 1, 2, 6, 1 }; ##EMO ANG 6
all_info[2][2][65] = { 1, 1, 1, 9, 0 };
all_info[2][2][66] = { 2, 1, 1, 3, 0 };
all_info[2][2][67] = { 1, 1, 1, 11, 0 };
all_info[2][2][68] = { 1, 1, 3, 5, 0 };
all_info[2][2][69] = { 1, 1, 3, 9, 1 }; ##EMO F 9
all_info[2][2][70] = { 1, 1, 2, 4, 0 };
all_info[2][2][71] = { 2, 1, 1, 11, 0 };
all_info[2][2][72] = { 1, 1, 1, 1, 0 };
all_info[2][2][73] = { 1, 1, 3, 9, 0 };
all_info[2][2][74] = { 2, 1, 1, 12, 0 };
all_info[2][2][75] = { 1, 1, 2, 2, 0 };
all_info[2][2][76] = { 1, 1, 1, 2, 2 }; ##FID N 2
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~ SIXTH ORDER ~~~~~~~~~~~~~~~~~
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
all_info[2][3][1] = {2, 1, 2, 1, 0};
all_info[2][3][2] = {1, 1, 1, 2, 0};
all_info[2][3][3] = {1, 1, 3, 12, 0};
all_info[2][3][4] = {1, 1, 1, 11, 0};
all_info[2][3][5] = {1, 1, 3, 11, 2}; #FID F 11
all_info[2][3][6] = {2, 1, 2, 2, 0};
all_info[2][3][7] = {1, 1, 2, 6, 0};
all_info[2][3][8] = {2, 1, 2, 1, 0};
all_info[2][3][9] = {1, 1, 1, 11, 0};
all_info[2][3][10] = {1, 1, 2, 8, 0};
all_info[2][3][11] = {1, 1, 2, 1, 1}; #EMO A 1
all_info[2][3][12] = {1, 1, 1, 4, 0};
all_info[2][3][13] = {1, 1, 3, 7, 0};
all_info[2][3][14] = {1, 1, 1, 1, 0};
all_info[2][3][15] = {2, 1, 2, 6, 0};
all_info[2][3][16] = {2, 1, 2, 6, 3}; #SCRAMBLED A 6
all_info[2][3][17] = {1, 1, 1, 1, 0};
all_info[2][3][18] = {2, 1, 1, 4, 0};
all_info[2][3][19] = {1, 1, 3, 10, 0};
all_info[2][3][20] = {2, 1, 2, 5, 0};
all_info[2][3][21] = {1, 1, 2, 8, 0};
all_info[2][3][22] = {1, 1, 3, 10, 0};
all_info[2][3][23] = {1, 1, 2, 4, 0};
all_info[2][3][24] = {2, 1, 3, 8, 0};
all_info[2][3][25] = {1, 1, 1, 4, 0};
all_info[2][3][26] = {2, 1, 1, 10, 0};
all_info[2][3][27] = {2, 1, 1, 10, 3}; #SCRAMBLED NEUT 10
all_info[2][3][28] = {1, 1, 3, 9, 0};
all_info[2][3][29] = {1, 1, 2, 5, 0};
all_info[2][3][30] = {1, 1, 3, 10, 0};
all_info[2][3][31] = {1, 1, 2, 5, 0};
all_info[2][3][32] = {2, 1, 3, 7, 0};
all_info[2][3][33] = {1, 1, 3, 12, 0};
all_info[2][3][34] = {2, 1, 3, 9, 0};
all_info[2][3][35] = {2, 1, 3, 9, 3}; #SCRAMBLED F 9
all_info[2][3][36] = {1, 1, 2, 2, 0};
all_info[2][3][37] = {1, 1, 1, 9, 0};
all_info[2][3][38] = {1, 1, 3, 9, 2}; #FID F 9
all_info[2][3][39] = {1, 1, 1, 2, 0};
all_info[2][3][40] = {2, 2, 1, 11, 0};
all_info[2][3][41] = {1, 1, 1, 3, 0};
all_info[2][3][42] = {1, 1, 3, 5, 0};
all_info[2][3][43] = {1, 1, 2, 6, 0};
all_info[2][3][44] = {1, 1, 1, 9, 0};
all_info[2][3][45] = {1, 1, 1, 3, 1}; #EMO NEUT 3
all_info[2][3][46] = {2, 1, 1, 11, 0};
all_info[2][3][47] = {1, 1, 3, 12, 0};
all_info[2][3][48] = {1, 1, 2, 7, 0};
all_info[2][3][49] = {1, 1, 1, 11, 0};
all_info[2][3][50] = {1, 1, 1, 12, 1}; #EMO N 12 
all_info[2][3][51] = {1, 1, 2, 3, 0};
all_info[2][3][52] = {1, 1, 3, 7, 0};
all_info[2][3][53] = {2, 1, 1, 1, 0};
all_info[2][3][54] = {1, 1, 3, 6, 0};
all_info[2][3][55] = {1, 1, 2, 4, 0};
all_info[2][3][56] = {1, 1, 3, 8, 0};
all_info[2][3][57] = {1, 1, 2, 8, 2}; #FID A 8
all_info[2][3][58] = {1, 1, 3, 11, 0};
all_info[2][3][59] = {2, 1, 1, 9, 0};
all_info[2][3][60] = {1, 1, 3, 9, 0};
all_info[2][3][61] = {1, 1, 1, 10, 0};
all_info[2][3][62] = {1, 1, 3, 8, 0};
all_info[2][3][63] = {1, 1, 2, 3, 0};
all_info[2][3][64] = {1, 1, 3, 5, 0};
all_info[2][3][65] = {1, 1, 1, 10, 0};
all_info[2][3][66] = {2, 1, 3, 10, 0};
all_info[2][3][67] = {2, 1, 3, 10, 3}; #SCRAMBLED F 10
all_info[2][3][68] = {1, 1, 3, 7, 0};
all_info[2][3][69] = {1, 1, 2, 7, 2}; # FID ANG 7
all_info[2][3][70] = {1, 1, 1, 1, 0};
all_info[2][3][71] = {1, 1, 2, 2, 0};
all_info[2][3][72] = {1, 1, 1, 3, 0};
all_info[2][3][73] = {1, 1, 2, 6, 0};
all_info[2][3][74] = {1, 1, 2, 4, 1}; #EMO A 4
all_info[2][3][75] = {2, 1, 3, 11, 0};
all_info[2][3][76] = {1, 1, 2, 5, 0};
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEVENTH ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
all_info[3][1][1]={1,2,2,6,0};
all_info[3][1][2]={1,2,3,9,0};
all_info[3][1][3]={2,2,1,4,0};
all_info[3][1][4]={1,2,2,2,0};
all_info[3][1][5]={1,2,3,7,0};
all_info[3][1][6]={1,2,2,3,0};
all_info[3][1][7]={1,2,1,10,0};
all_info[3][1][8]={1,2,2,5,0};
all_info[3][1][9]={1,2,3,5,2}; #FID F 5
all_info[3][1][10]={1,2,1,4,0};
all_info[3][1][11]={2,2,1,1,0};
all_info[3][1][12]={1,2,1,9,0};
all_info[3][1][13]={1,2,2,8,1}; #EMO ANG 8
all_info[3][1][14]={2,2,2,3,0};
all_info[3][1][15]={1,2,3,11,0};
all_info[3][1][16]={2,2,3,6,0};
all_info[3][1][17]={1,2,3,10,0};
all_info[3][1][18]={1,2,1,3,0};
all_info[3][1][19]={1,2,3,5,0};
all_info[3][1][20]={1,2,3,8,1}; #EMO F 8
all_info[3][1][21]={1,2,1,1,0};
all_info[3][1][22]={1,2,2,5,0};
all_info[3][1][23]={1,2,1,12,0};
all_info[3][1][24]={2,2,2,5,0};
all_info[3][1][25]={1,2,1,1,0};
all_info[3][1][26]={1,2,2,7,0};
all_info[3][1][27]={2,2,1,11,0};
all_info[3][1][28]={2,2,1,11,3}; #SCRAMBLED  F 6
all_info[3][1][29]={1,2,2,7,0};
all_info[3][1][30]={1,2,3,6,0};
all_info[3][1][31]={2,2,1,10,0};
all_info[3][1][32]={1,2,2,1,0};
all_info[3][1][33]={2,2,2,2,0};
all_info[3][1][34]={1,2,3,10,0};
all_info[3][1][35]={2,2,2,1,0};
all_info[3][1][36]={1,2,3,12,0};
all_info[3][1][37]={2,2,3,8,0};
all_info[3][1][38]={2,2,3,8,3}; #SCRAMBLED  F 8
all_info[3][1][39]={1,2,2,3,0};
all_info[3][1][40]={2,2,3,11,0};
all_info[3][1][41]={1,2,3,11,0};
all_info[3][1][42]={1,2,1,11,2}; #FID  NEUT 11
all_info[3][1][43]={1,2,2,7,0};
all_info[3][1][44]={1,2,3,6,0};
all_info[3][1][45]={1,2,1,9,0};
all_info[3][1][46]={1,2,2,8,0};
all_info[3][1][47]={1,2,3,8,2}; #FID F 8
all_info[3][1][48]={1,2,1,4,0};
all_info[3][1][49]={2,2,3,12,0};
all_info[3][1][50]={2,2,3,12,3}; #SCRAMBLED  F 12
all_info[3][1][51]={1,2,2,4,0};
all_info[3][1][52]={2,2,3,9,0};
all_info[3][1][53]={1,2,2,1,0};
all_info[3][1][54]={1,2,1,9,0};
all_info[3][1][55]={1,2,1,3,1}; #EMO  N 3
all_info[3][1][56]={1,2,3,9,0};
all_info[3][1][57]={1,2,1,2,0};
all_info[3][1][58]={1,2,2,2,2}; #FID A 2
all_info[3][1][59]={2,2,1,2,0};
all_info[3][1][60]={1,2,3,7,0};
all_info[3][1][61]={1,2,1,12,0};
all_info[3][1][62]={1,2,1,1,1}; #EMO N 1
all_info[3][1][63]={1,2,3,8,0};
all_info[3][1][64]={1,2,2,4,0};
all_info[3][1][65]={1,2,1,10,0};
all_info[3][1][66]={1,2,2,6,0};
all_info[3][1][67]={1,2,1,3,0};
all_info[3][1][68]={2,2,1,3,0};
all_info[3][1][69]={1,2,1,2,0};
all_info[3][1][70]={1,2,3,11,0};
all_info[3][1][71]={1,2,2,2,0};
all_info[3][1][72]={1,2,3,12,0};
all_info[3][1][73]={1,2,1,11,0};
all_info[3][1][74]={2,2,2,4,0};
all_info[3][1][75]={2,2,2,4,3}; #SCRAMBLED A 4
all_info[3][1][76]={1,2,3,5,0};
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~~~~~~~~~~~~ EIGHTH ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
all_info[3][2][1]={2,2,1,1,0};
all_info[3][2][2]={2,2,1,1,3}; #SCRAMBLED  N 1
all_info[3][2][3]={1,2,2,5,0};
all_info[3][2][4]={1,2,2,6,0};
all_info[3][2][5]={1,2,2,3,1}; #EMO ANG 3
all_info[3][2][6]={1,2,2,4,0};
all_info[3][2][7]={2,2,2,2,0};
all_info[3][2][8]={1,2,3,6,0};
all_info[3][2][9]={1,2,1,11,0};
all_info[3][2][10]={1,2,2,2,0};
all_info[3][2][11]={1,2,1,3,0};
all_info[3][2][12]={2,2,1,3,0};
all_info[3][2][13]={1,2,1,4,0};
all_info[3][2][14]={1,2,1,2,1}; #EMO N 2
all_info[3][2][15]={2,2,3,7,0};
all_info[3][2][16]={1,2,3,11,0};
all_info[3][2][17]={1,2,2,3,0};
all_info[3][2][18]={2,2,3,7,0};
all_info[3][2][19]={1,2,3,8,0};
all_info[3][2][20]={2,2,2,6,0};
all_info[3][2][21]={2,2,2,6,3}; #SCRAMBLED  N 6
all_info[3][2][22]={1,2,2,1,0};
all_info[3][2][23]={2,2,3,5,0};
all_info[3][2][24]={1,2,3,7,0};
all_info[3][2][25]={1,2,2,7,2}; #FID N 7
all_info[3][2][26]={1,2,1,2,0};
all_info[3][2][27]={1,2,3,8,0};
all_info[3][2][28]={1,2,2,6,0};
all_info[3][2][29]={1,2,1,10,0};
all_info[3][2][30]={1,2,2,3,0};
all_info[3][2][31]={1,2,3,9,0};
all_info[3][2][32]={1,2,2,1,0};
all_info[3][2][33]={1,2,3,10,0};
all_info[3][2][34]={1,2,2,5,0};
all_info[3][2][35]={1,2,3,7,0};
all_info[3][2][36]={2,2,1,2,0};
all_info[3][2][37]={1,2,2,6,0};
all_info[3][2][38]={1,2,3,12,0};
all_info[3][2][39]={1,2,1,12,2}; #FID N 12
all_info[3][2][40]={1,2,2,7,0};
all_info[3][2][41]={1,2,1,4,0};
all_info[3][2][42]={2,2,1,9,0};
all_info[3][2][43]={1,2,2,8,0};
all_info[3][2][44]={1,2,1,10,0};
all_info[3][2][45]={2,2,3,10,0};
all_info[3][2][46]={2,2,3,10,3}; #SCRAMBLED F 10
all_info[3][2][47]={1,2,3,12,0};
all_info[3][2][48]={1,2,2,4,0};
all_info[3][2][49]={1,2,1,4,2}; #FID N 4
all_info[3][2][50]={1,2,3,11,0};
all_info[3][2][51]={1,2,1,1,0};
all_info[3][2][52]={2,2,3,11,0};
all_info[3][2][53]={1,2,1,11,0};
all_info[3][2][54]={1,2,3,10,0};
all_info[3][2][55]={1,2,1,9,0};
all_info[3][2][56]={2,2,1,12,0};
all_info[3][2][57]={1,2,1,12,0};
all_info[3][2][58]={1,2,3,5,0};
all_info[3][2][59]={1,2,3,12,1}; #EMO F 12
all_info[3][2][60]={1,2,2,2,0};
all_info[3][2][61]={1,2,1,12,0};
all_info[3][2][62]={1,2,3,9,0};
all_info[3][2][63]={1,2,1,9,2}; #FID N 9
all_info[3][2][64]={2,2,2,5,0};
all_info[3][2][65]={1,2,2,8,0};
all_info[3][2][66]={2,2,2,6,0};
all_info[3][2][67]={1,2,1,1,0};
all_info[3][2][68]={2,2,2,7,0};
all_info[3][2][69]={1,2,3,6,0};
all_info[3][2][70]={1,2,1,2,0};
all_info[3][2][71]={1,2,2,4,0};
all_info[3][2][72]={1,2,3,10,0};
all_info[3][2][73]={1,2,3,5,1}; #EMO F 5
all_info[3][2][74]={1,2,1,3,0};
all_info[3][2][75]={2,2,2,8,0};
all_info[3][2][76]={2,2,2,8,3}; #SCRAMBLED A 8
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~~~~~~~~~~~~ NINTH ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
all_info[3][3][1] = {1, 2, 2, 4, 0 };
all_info[3][3][2] = {1, 2, 3, 5, 0 };
all_info[3][3][3] = {1, 2, 2, 8, 0 }; 
all_info[3][3][4] = {1, 2, 3, 5, 0 };
all_info[3][3][5] = {1, 2, 1, 4, 0 };
all_info[3][3][6] = {1, 2, 2, 2, 0 }; 
all_info[3][3][7] = {2, 2, 1, 9, 0 };
all_info[3][3][8] = {1, 2, 2, 5, 0};
all_info[3][3][9] = {1, 2, 1, 12, 0}; 
all_info[3][3][10] = {1, 2, 3, 12, 2 }; #FID  F 12
all_info[3][3][11] = {2, 2, 3, 8, 0};
all_info[3][3][12] = {1, 2, 2, 1, 0};
all_info[3][3][13] = {1, 2, 1, 4, 0};
all_info[3][3][14] = {2, 2, 2, 2, 0};
all_info[3][3][15] = {1, 2, 1, 1, 0 };
all_info[3][3][16] = {2, 2, 1, 4, 0 };
all_info[3][3][17] = {2, 2, 1, 4, 3 }; #SCRAMBLED N 4
all_info[3][3][18] = {1, 2, 2, 1, 0 };
all_info[3][3][19] = {1, 2, 1, 10, 0 };
all_info[3][3][20] = {1, 2, 3, 6, 0};
all_info[3][3][21] = {1, 2, 2, 6, 2 }; #FID ANG 6
all_info[3][3][22] = {1, 2, 1, 9, 0};
all_info[3][3][23] = {1, 2, 3, 6, 0 };
all_info[3][3][24] = {1, 2, 2, 5, 0 }; 
all_info[3][3][25] = {2, 2, 2, 7, 0 };
all_info[3][3][26] = {1, 2, 1, 1, 0};
all_info[3][3][27] = {1, 2, 3, 8, 0}; 
all_info[3][3][28] = {1, 2, 2, 8, 2}; #FID A 8
all_info[3][3][29] = {1, 2, 1, 12, 0 };
all_info[3][3][30] = {1, 2, 2, 7, 0 };
all_info[3][3][31] = {1, 2, 2, 6, 1 }; #EMO A 6
all_info[3][3][32] = {1, 2, 1, 11, 0 };
all_info[3][3][33] = {1, 2, 3, 7, 0 };
all_info[3][3][34] = {2, 2, 3, 6, 0 };
all_info[3][3][35] = {1, 2, 3, 9, 0 };
all_info[3][3][36] = {2, 2, 2, 7, 0 };
all_info[3][3][37] = {1, 2, 1, 10, 0 };
all_info[3][3][38] = {1, 2, 3, 10, 2}; #FID FEAR 10
all_info[3][3][39] = {2, 2, 3, 9, 0};
all_info[3][3][40] = {1, 2, 3, 6, 0};
all_info[3][3][41] = {2, 2, 3, 9, 0};
all_info[3][3][42] = {1, 2, 3, 11, 0};
all_info[3][3][43] = {1, 2, 1, 3, 0};
all_info[3][3][44] = {1, 2, 1, 11, 1 }; #EMO N 11
all_info[3][3][45] = {1, 2, 3, 9, 0 };
all_info[3][3][46] = {1, 2, 2, 7, 0 };
all_info[3][3][47] = {1, 2, 3, 12, 0}; 
all_info[3][3][48] = {1, 2, 2, 1, 0 };
all_info[3][3][49] = {1, 2, 3, 7, 0};
all_info[3][3][50] = {2, 2, 1, 10, 0}; 
all_info[3][3][51] = {2, 2, 1, 10, 3 };#SCRAMBLED  N 10
all_info[3][3][52] = {1, 2, 1, 2, 0};
all_info[3][3][53] = {2, 2, 1, 11, 0};
all_info[3][3][54] = {1, 2, 1, 3, 0}; 
all_info[3][3][55] = {1, 2, 2, 8, 0};
all_info[3][3][56] = {2, 2, 3, 12, 0};
all_info[3][3][57] = {1, 2, 3, 7, 0 };
all_info[3][3][58] = {1, 2, 2, 4, 0 };
all_info[3][3][59] = {1, 2, 1, 2, 0 };
all_info[3][3][60] = {1, 2, 3, 9, 0};
all_info[3][3][61] = {1, 2, 3, 10, 1 }; #EMO F 9
all_info[3][3][62] = {2, 2, 2, 1, 0};
all_info[3][3][63] = {1, 2, 2, 3, 0};
all_info[3][3][64] = {2, 2, 3, 5, 0}; 
all_info[3][3][65] = {2, 2, 3, 5, 3 }; #SCRAMBLED  FEAr 5
all_info[3][3][66] = {1, 2, 1, 10, 0};
all_info[3][3][67] = {1, 2, 2, 2, 0};
all_info[3][3][68] = {1, 2, 2, 5, 1 };  #EMO A 5
all_info[3][3][69] = {1, 2, 3, 8, 0 };
all_info[3][3][70] = {2, 2, 2, 3, 0};
all_info[3][3][71] = {2, 2, 2, 3, 3 }; #SCRAMBLED A 3
all_info[3][3][72] = { 1, 2, 1, 11, 0};
all_info[3][3][73] = {1, 2, 2, 3, 0 };
all_info[3][3][74] = {2, 2, 1, 12, 0 }; 
all_info[3][3][75] = {1, 2, 1, 9, 0};
all_info[3][3][76] = {1, 2, 3, 11, 0 };
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~~~~~~~~~~~~ TENTH ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
all_info[4][1][1]={2,2,1,2,0};
all_info[4][1][2]={2,2,1,2,3}; #SCRAMBLED  N 2
all_info[4][1][3]={1,2,3,11,0};
all_info[4][1][4]={1,2,1,1,0};
all_info[4][1][5]={2,2,2,2,0};
all_info[4][1][6]={2,2,2,2,3}; #SCRAMBLED A 2
all_info[4][1][7]={1,2,1,9,0};
all_info[4][1][8]={1,2,3,8,0};
all_info[4][1][9]={1,2,3,9,1}; #EMO F 9
all_info[4][1][10]={2,2,2,4,0};
all_info[4][1][11]={1,2,1,4,0};
all_info[4][1][12]={1,2,2,6,0};
all_info[4][1][13]={1,2,3,6,2}; #FID F 6
all_info[4][1][14]={1,2,1,11,0};
all_info[4][1][15]={1,2,2,5,0};
all_info[4][1][16]={1,2,1,4,0};
all_info[4][1][17]={1,2,3,9,0};
all_info[4][1][18]={1,2,2,2,0};
all_info[4][1][19]={2,2,1,11,0};
all_info[4][1][20]={1,2,2,3,0};
all_info[4][1][21]={2,2,3,5,0};
all_info[4][1][22]={1,2,3,8,0};
all_info[4][1][23]={1,2,2,7,0};
all_info[4][1][24]={1,2,3,6,0};
all_info[4][1][25]={2,2,1,10,0};
all_info[4][1][26]={1,2,3,10,0};
all_info[4][1][27]={1,2,1,10,2}; #FID NEUT 10
all_info[4][1][28]={1,2,2,3,0};
all_info[4][1][29]={1,2,3,7,0};
all_info[4][1][30]={1,2,2,8,0};
all_info[4][1][31]={1,2,2,4,1}; #EMO A 4
all_info[4][1][32]={2,2,3,12,0};
all_info[4][1][33]={1,2,2,7,0};
all_info[4][1][34]={1,2,3,7,2}; #FID FEAR 7
all_info[4][1][35]={1,2,1,1,0};
all_info[4][1][36]={2,2,3,8,0};
all_info[4][1][37]={1,2,3,5,0};
all_info[4][1][38]={1,2,2,5,2}; #FID A 5
all_info[4][1][39]={1,2,1,10,0};
all_info[4][1][40]={1,2,3,5,0};
all_info[4][1][41]={2,2,1,9,0};
all_info[4][1][42]={1,2,2,1,0};
all_info[4][1][43]={1,2,1,9,0};
all_info[4][1][44]={2,2,3,10,0};
all_info[4][1][45]={1,2,3,12,0};
all_info[4][1][46]={1,2,2,2,0};
all_info[4][1][47]={1,2,3,6,0};
all_info[4][1][48]={2,2,3,11,0};
all_info[4][1][49]={2,2,3,11,3}; #SCRAMBLED F 11
all_info[4][1][50]={1,2,2,5,0};
all_info[4][1][51]={1,2,1,3,0};
all_info[4][1][52]={1,2,3,10,0};
all_info[4][1][53]={2,2,1,12,0};
all_info[4][1][54]={1,2,1,2,0};
all_info[4][1][55]={1,2,3,11,0};
all_info[4][1][56]={1,2,1,3,0};
all_info[4][1][57]={1,2,3,12,0};
all_info[4][1][58]={1,2,2,1,0};
all_info[4][1][59]={1,2,1,12,0};
all_info[4][1][60]={1,2,2,6,0};
all_info[4][1][61]={1,2,3,12,0};
all_info[4][1][62]={1,2,1,11,0};
all_info[4][1][63]={1,2,1,4,1}; #EMO N 4
all_info[4][1][64]={1,2,2,1,0};
all_info[4][1][65]={1,2,3,10,0};
all_info[4][1][66]={1,2,2,7,0};
all_info[4][1][67]={2,2,2,5,0};
all_info[4][1][68]={1,2,2,8,0};
all_info[4][1][69]={2,2,2,6,0};
all_info[4][1][70]={1,2,1,2,0};
all_info[4][1][71]={1,2,1,12,1}; #EMO N 12
all_info[4][1][72]={1,2,2,4,0};
all_info[4][1][73]={2,2,1,1,0};
all_info[4][1][74]={1,2,1,11,0};
all_info[4][1][75]={2,2,2,7,0};
all_info[4][1][76]={2,2,2,7,3}; #SCRAMBLED ANG 7
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~~~~~~~~~~~~ ELEVENTH ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
all_info[4][2][1] = {1, 2, 3, 5, 0};
all_info[4][2][2] = {1, 2, 3, 6, 1}; #EMO F 6
all_info[4][2][3] = {1, 2, 2, 3, 0};
all_info[4][2][4] = {2, 2, 1, 1, 0};
all_info[4][2][5] = {1, 2, 1, 3, 0};
all_info[4][2][6] = {2, 2, 3, 10, 0};
all_info[4][2][7] = {1, 2, 1, 9, 0};
all_info[4][2][8] = {1, 2, 3, 9, 2}; #FID F 9
all_info[4][2][9] = {2, 2, 2, 3, 0};
all_info[4][2][10] = {1, 2, 1, 10, 0};
all_info[4][2][11] = {2, 2, 1, 4, 0};
all_info[4][2][12] = {1, 2, 3, 10, 0}; 
all_info[4][2][13] = {2, 2, 3, 6, 0}; 
all_info[4][2][14] = {2, 2, 3, 6, 3}; #SCRAMBLED F 5
all_info[4][2][15] = {1, 2, 3, 10, 0};
all_info[4][2][16] = {1, 2, 1, 9, 0};
all_info[4][2][17] = {2, 2, 2, 4, 0};
all_info[4][2][18] = {1, 2, 3, 8, 0};
all_info[4][2][19] = {1, 2, 2, 2, 0}; 
all_info[4][2][20] = {1, 2, 1, 2, 2}; # FID NEUT 2
all_info[4][2][21] = {1, 2, 2, 8, 0};
all_info[4][2][22] = {2, 2, 2, 4, 0};
all_info[4][2][23] = {1, 2, 1, 4, 0};
all_info[4][2][24] = {1, 2, 2, 7, 0};
all_info[4][2][25] = {1, 2, 1, 3, 0};
all_info[4][2][26] = {1, 2, 3, 12, 0};
all_info[4][2][27] = {1, 2, 1, 3, 0};
all_info[4][2][28] = {1, 2, 3, 9, 0};
all_info[4][2][29] = {1, 2, 3, 11, 1}; #EMO F 8
all_info[4][2][30] = {1, 2, 2, 4, 0};
all_info[4][2][31] = {2, 2, 2, 8, 0};
all_info[4][2][32] = {1, 2, 2, 4, 0};
all_info[4][2][33] = {1, 2, 3, 6, 0};
all_info[4][2][34] = {1, 2, 1, 4, 0};
all_info[4][2][35] = {1, 2, 2, 4, 2}; #FID A 4
all_info[4][2][36] = {2, 2, 3, 6, 0};
all_info[4][2][37] = {1, 2, 2, 8, 0};
all_info[4][2][38] = {2, 2, 3, 7, 0};
all_info[4][2][39] = {2, 2, 3, 7, 3}; #SCRAMBLED FEAR 7
all_info[4][2][40] = {1, 2, 3, 8, 0};
all_info[4][2][41] = {2, 2, 2, 8, 0};
all_info[4][2][42] = {1, 2, 2, 5, 0}; 
all_info[4][2][43] = {1, 2, 3, 12, 0};
all_info[4][2][44] = {1, 2, 1, 1, 0};
all_info[4][2][45] = {1, 2, 2, 6, 0};
all_info[4][2][46] = {1, 2, 1, 11, 0};
all_info[4][2][47] = {1, 2, 2, 8, 0};
all_info[4][2][48] = {1, 2, 1, 2, 0};
all_info[4][2][49] = {1, 2, 1, 10, 1}; #EMO N 2
all_info[4][2][50] = {1, 2, 2, 6, 0};
all_info[4][2][51] = {2, 2, 1, 12, 0};
all_info[4][2][52] = {2, 2, 1, 12, 3}; #SCRAMBLED N 12
all_info[4][2][53] = {1, 2, 2, 5, 0}; 
all_info[4][2][54] = {1, 2, 1, 12, 0};
all_info[4][2][55] = {1, 2, 2, 1, 0};
all_info[4][2][56] = {1, 2, 1, 1, 2}; #FID N 1
all_info[4][2][57] = {1, 2, 3, 11, 0};
all_info[4][2][58] = {2, 2, 2, 1, 0};
all_info[4][2][59] = {1, 2, 1, 2, 0};
all_info[4][2][60] = {1, 2, 3, 5, 0}; 
all_info[4][2][61] = {1, 2, 2, 6, 0};
all_info[4][2][62] = {1, 2, 1, 12, 0};
all_info[4][2][63] = {1, 2, 3, 9, 0};
all_info[4][2][64] = {1, 2, 2, 2, 0};
all_info[4][2][65] = {2, 2, 3, 10, 0};
all_info[4][2][66] = {1, 2, 1, 1, 0};
all_info[4][2][67] = {2, 2, 1, 3, 0};
all_info[4][2][68] = {2, 2, 1, 3, 3}; #SCRAMBLED N 3
all_info[4][2][69] = {1, 2, 2, 3, 0};
all_info[4][2][70] = {1, 2, 3, 7, 0};
all_info[4][2][71] = {2, 2, 1, 4, 0};
all_info[4][2][72] = {1, 2, 2, 2, 0};
all_info[4][2][73] = {1, 2, 3, 5, 0}; 
all_info[4][2][74] = {1, 2, 2, 7, 0};
all_info[4][2][75] = {1, 2, 1, 11, 0};
all_info[4][2][76] = {1, 2, 3, 7, 1}; #EMO F 7
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#~~~~~~~~~~~~~~~~~~~~~~~~~~~ TWELFTH ORDER ~~~~~~~~~~~~~~~~~~~~~~~~~~~
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
all_info[4][3][1] = { 2, 2, 1, 3, 0 };
all_info[4][3][2] = { 1, 2, 2, 5, 0 };
all_info[4][3][3] = { 1, 2, 2, 7, 1 }; ##EMO A 7
all_info[4][3][4] = { 1, 2, 3, 12, 0 };
all_info[4][3][5] = { 1, 2, 2, 8, 0 };
all_info[4][3][6] = { 1, 2, 1, 12, 0 };
all_info[4][3][7] = { 1, 2, 2, 1, 0 };
all_info[4][3][8] = { 1, 2, 2, 2, 1 }; ##EMO A 2
all_info[4][3][9] = { 1, 2, 3, 9, 0 };
all_info[4][3][10] = { 2, 2, 1, 2, 0 };
all_info[4][3][11] = { 1, 2, 3, 9, 0 };
all_info[4][3][12] = { 2, 2, 2, 3, 0 };
all_info[4][3][13] = { 1, 2, 1, 1, 0 };
all_info[4][3][14] = { 1, 2, 3, 10, 0 };
all_info[4][3][15] = { 2, 2, 1, 9, 0 };
all_info[4][3][16] = { 2, 2, 1, 9, 3 }; ##SCRAMB N 9
all_info[4][3][17] = { 1, 2, 3, 6, 0 };
all_info[4][3][18] = { 2, 2, 3, 5, 0 };
all_info[4][3][19] = { 1, 2, 2, 8, 0 };
all_info[4][3][20] = { 1, 2, 1, 4, 0 };
all_info[4][3][21] = { 1, 2, 1, 9, 1 }; ##EMO NEUT 9
all_info[4][3][22] = { 1, 2, 3, 5, 0 };
all_info[4][3][23] = { 1, 2, 1, 9, 0 };
all_info[4][3][24] = { 1, 2, 3, 8, 0 };
all_info[4][3][25] = { 2, 2, 2, 6, 0 };
all_info[4][3][26] = { 1, 2, 1, 12, 0 };
all_info[4][3][27] = { 1, 2, 3, 10, 0 };
all_info[4][3][28] = { 1, 2, 1, 3, 0 };
all_info[4][3][29] = { 1, 2, 2, 3, 2 }; ##FID A 3
all_info[4][3][30] = { 1, 2, 3, 5, 0 };
all_info[4][3][31] = { 1, 2, 2, 6, 0 };
all_info[4][3][32] = { 1, 2, 3, 7, 0 }; 
all_info[4][3][33] = { 2, 2, 2, 7, 0 };
all_info[4][3][34] = { 1, 2, 1, 4, 0 }; 
all_info[4][3][35] = { 2, 2, 2, 1, 0 };
all_info[4][3][36] = { 2, 2, 2, 1, 3 }; ##SCRAMBLED ANG 1
all_info[4][3][37] = { 1, 2, 1, 10, 0 };
all_info[4][3][38] = { 1, 2, 2, 4, 0 }; 
all_info[4][3][39] = { 1, 2, 1, 10, 0 }; 
all_info[4][3][40] = { 1, 2, 2, 4, 0 };
all_info[4][3][41] = { 1, 2, 3, 8, 0 };
all_info[4][3][42] = { 1, 2, 2, 3, 0 };
all_info[4][3][43] = { 1, 2, 2, 1, 1 }; ##EMO A 1
all_info[4][3][44] = { 1, 2, 1, 10, 0 };
all_info[4][3][45] = { 1, 2, 3, 12, 0 };
all_info[4][3][46] = { 1, 2, 1, 1, 0 };
all_info[4][3][47] = { 1, 2, 2, 1, 2 }; ##FID A 1
all_info[4][3][48] = { 2, 2, 1, 10, 0 };
all_info[4][3][49] = { 1, 2, 1, 11, 0 };
all_info[4][3][50] = { 1, 2, 2, 8, 0 };
all_info[4][3][51] = { 1, 2, 3, 6, 0 };
all_info[4][3][52] = { 2, 2, 3, 8, 0 };
all_info[4][3][53] = { 1, 2, 3, 7, 0 };
all_info[4][3][54] = { 1, 2, 1, 12, 0 };
all_info[4][3][55] = { 2, 2, 3, 11, 0 };
all_info[4][3][56] = { 1, 2, 2, 6, 0 };
all_info[4][3][57] = { 1, 2, 3, 11, 0 };
all_info[4][3][58] = { 1, 2, 1, 2, 0 };
all_info[4][3][59] = { 2, 2, 2, 5, 0 };
all_info[4][3][60] = { 2, 2, 2, 5, 3 }; ##SCRAM ANG 5
all_info[4][3][61] = { 1, 2, 2, 5, 0 };
all_info[4][3][62] = { 1, 2, 3, 7, 0 };
all_info[4][3][63] = { 1, 2, 2, 3, 0 };
all_info[4][3][64] = { 1, 2, 1, 3, 2 }; ## FID NEUT 3
all_info[4][3][65] = { 1, 2, 3, 7, 0 };
all_info[4][3][66] = { 1, 2, 1, 2, 0 };
all_info[4][3][67] = { 2, 2, 3, 9, 0 };
all_info[4][3][68] = { 2, 2, 3, 9, 3 }; ##SCRAM F 9
all_info[4][3][69] = { 1, 2, 3, 11, 0 };
all_info[4][3][70] = { 2, 2, 3, 8, 0 };
all_info[4][3][71] = { 1, 2, 1, 11, 0 };
all_info[4][3][72] = { 1, 2, 3, 11, 2 }; # FID FEAR 11
all_info[4][3][73] = { 2, 2, 1, 11, 0 };
all_info[4][3][74] = { 1, 2, 1, 9, 0 };
all_info[4][3][75] = { 1, 2, 2, 2, 0 };
all_info[4][3][76] = { 2, 2, 3, 12, 0 };

# --- Set up fixed stimulus parameters ---
# Create array of stimulus arrays
array<string> instr[16][4];           # AS/ seems to be the String Array of instructions. 
array<int> block_order[16][8][3][76][5];
int task_number = 0;
string language = parameter_manager.get_string( "Language" );
bool char_wrap = false;
double font_size = parameter_manager.get_double( "Non-Stimulus Font Size" );

# Set up the rest trial 
int rest_dur = parameter_manager.get_int( "Rest Break Duration" );
if ( rest_dur > 0 ) then
	rest_trial.set_type( rest_trial.FIXED );
	trial_refresh_fix( rest_trial, rest_dur );
end;
string rest_cap = "Please take a real break! Press the top-right key when ready.";
full_size_word_wrap( rest_cap, font_size, char_wrap, rest_text );

# Add fixation to ISI
if ( parameter_manager.get_bool( "Show Fixation Point During ISI" ) ) then
	ISI_pic.add_part( fix_circ, 0, 0 );
end;

# Change response logging
if ( parameter_manager.get_string( "Response Logging" ) == LOG_ACTIVE ) then
	ISI_trial.set_all_responses( false );
	stim_trial.set_all_responses( false );
end;

# Setup Counterbalancing Scheme (1-16)
int block_config = parameter_manager.get_int( "Counterbalance Scheme" );

# --- SUBROUTINES --- #
# --- sub present_instructions 
sub
	present_instructions( string instruct_string )
begin
	full_size_word_wrap( instruct_string, font_size, char_wrap, instruct_text );
	instruct_trial.present();
	default.present();
end;
# --- sub ready_set_go ---

int ready_dur = parameter_manager.get_int( "Ready-Set-Go Duration" );
trial_refresh_fix( ready_trial, ready_dur );

array<string> ready_caps[3];
ready_caps[1] = "Ready";
ready_caps[2] = "Set";
ready_caps[3] = "Go!";

sub
	ready_set_go
begin
	if ( ready_dur > 0 ) then
		loop
			int i = 1
		until
			i > ready_caps.count()
		begin
			full_size_word_wrap( ready_caps[i], font_size, char_wrap, ready_text );
			ready_trial.present();
			i = i + 1;
		end;
	end;
end;
# --- sub get_filename
sub
	string get_filename( bitmap this_bitmap )
begin
	string temp_string = this_bitmap.filename();
	
	int last_slash = 1;
	loop
	until
		temp_string.find( "\\", last_slash ) == 0
	begin
		last_slash = last_slash + 1;
	end;
	
	temp_string = temp_string.substring( last_slash, temp_string.count()-last_slash+1 );
	
	return temp_string
end;
# --- sub get_port_code

# stim_type = face  =1 or scrambled =2
# stim_age = Young =1 or Old		=2
# stim_emotion = neutral=1 or angry =2 or fearful =3
# 
# 1-49 : Caucasian Face
#	SetA: 1-8N - 17-24A - 33-40F
#	SetB: 9-16N - 25-32A - 41-48F
# 51-99 : Cacucasian Scrambled
#	SetA: 51-58N - 67-74A - 83-90F
#	SetB: 59-66N - 75-82A - 91-98F
# 101-149 : Old Face
#	SetA: 101-108N - 117-124A - 133-140F
#	SetB: 109-116N - 125-132A - 141-148F
# 151-199 : Old Scrambled
#	SetA: 151-158N - 167-174A - 183-190F
#	SetB: 159-166N - 175-182A - 191-198F
# 
#  YesID : 201  |  YesEMO : 202  |  YesScram: 203
#Response Codes:
#		Face ID:
#			YES > respond YES = HIT: 230
#			NO > respond YES = FALSE ALARM: 231
#		Emotion ID:
#			YES > respond YES = HIT: 240
#			NO > respond YES = FALSE ALARM: 241

sub
	int get_port_code( int stim_type, int stim_age, int stim_emotion, int model_num, string set )
begin
	string this_p_code = "";
	if( stim_type == 1 ) then # faces
		if( set == "A" ) then # setA faces
			if( stim_age == 1 ) then # Young setA faces
				if( stim_emotion == 1 ) then # neutral Young setA faces
					if( model_num == 1 ) then
						this_p_code = "1";
					elseif( model_num == 2 ) then
						this_p_code = "2";
					elseif( model_num == 3 ) then
						this_p_code = "3";
					elseif( model_num == 4 ) then
						this_p_code = "4";
					elseif( model_num == 9 ) then
						this_p_code = "5";
					elseif( model_num == 10 ) then
						this_p_code = "6";
					elseif( model_num == 11 ) then
						this_p_code = "7";
					elseif( model_num == 12 ) then
						this_p_code = "8";
					end;
				elseif( stim_emotion == 2 ) then # angry Young setA faces
					if( model_num == 1 ) then
						this_p_code = "17";
					elseif( model_num == 2 ) then
						this_p_code = "18";
					elseif( model_num == 3 ) then
						this_p_code = "19";
					elseif( model_num == 4 ) then
						this_p_code = "20";
					elseif( model_num == 5 ) then
						this_p_code = "21";
					elseif( model_num == 6 ) then
						this_p_code = "22";
					elseif( model_num == 7 ) then
						this_p_code = "23";
					elseif( model_num == 8 ) then
						this_p_code = "24";
					end;
				elseif( stim_emotion == 3 ) then # fearful Young setA faces
					if( model_num == 5 ) then
						this_p_code = "33";
					elseif( model_num == 6 ) then
						this_p_code = "34";
					elseif( model_num == 7 ) then
						this_p_code = "35";
					elseif( model_num == 8 ) then
						this_p_code = "36";
					elseif( model_num == 9 ) then
						this_p_code = "37";
					elseif( model_num == 10 ) then
						this_p_code = "38";
					elseif( model_num == 11 ) then
						this_p_code = "39";
					elseif( model_num == 12 ) then
						this_p_code = "40";
					end;
				end;
			elseif( stim_age == 2 ) then # Old set A faces
				if( stim_emotion == 1 ) then # neutral Old setA faces
					if( model_num == 1 ) then
						this_p_code = "101";
					elseif( model_num == 2 ) then
						this_p_code = "102";
					elseif( model_num == 3 ) then
						this_p_code = "103";
					elseif( model_num == 4 ) then
						this_p_code = "104";
					elseif( model_num == 9 ) then
						this_p_code = "105";
					elseif( model_num == 10 ) then
						this_p_code = "106";
					elseif( model_num == 11 ) then
						this_p_code = "107";
					elseif( model_num == 12 ) then
						this_p_code = "108";
					end;
				elseif( stim_emotion == 2 ) then # angry Old setA faces
					if( model_num == 1 ) then
						this_p_code = "117";
					elseif( model_num == 2 ) then
						this_p_code = "118";
					elseif( model_num == 3 ) then
						this_p_code = "119";
					elseif( model_num == 4 ) then
						this_p_code = "120";
					elseif( model_num == 5 ) then
						this_p_code = "121";
					elseif( model_num == 6 ) then
						this_p_code = "122";
					elseif( model_num == 7 ) then
						this_p_code = "123";
					elseif( model_num == 8 ) then
						this_p_code = "124";
					end;
				elseif( stim_emotion == 3 ) then # fearful Old setA faces
					if( model_num == 5 ) then
						this_p_code = "133";
					elseif( model_num == 6 ) then
						this_p_code = "134";
					elseif( model_num == 7 ) then
						this_p_code = "135";
					elseif( model_num == 8 ) then
						this_p_code = "136";
					elseif( model_num == 9 ) then
						this_p_code = "137";
					elseif( model_num == 10 ) then
						this_p_code = "138";
					elseif( model_num == 11 ) then
						this_p_code = "139";
					elseif( model_num == 12 ) then
						this_p_code = "140";
					end;
				end;
			end;
		else#if( set == "B" ) then # setB faces
			if( stim_age == 1 ) then # Young setB faces
				if( stim_emotion == 1 ) then # neutral Young setB faces
					if( model_num == 13 ) then
						this_p_code = "9";
					elseif( model_num == 14 ) then
						this_p_code = "10";
					elseif( model_num == 15 ) then
						this_p_code = "11";
					elseif( model_num == 16 ) then
						this_p_code = "12";
					elseif( model_num == 21 ) then
						this_p_code = "13";
					elseif( model_num == 22 ) then
						this_p_code = "14";
					elseif( model_num == 23 ) then
						this_p_code = "15";
					elseif( model_num == 24 ) then
						this_p_code = "16";
					end;
				elseif( stim_emotion == 2 ) then # angry Young setB faces
					if( model_num == 13 ) then
						this_p_code = "25";
					elseif( model_num == 14 ) then
						this_p_code = "26";
					elseif( model_num == 15 ) then
						this_p_code = "27";
					elseif( model_num == 16 ) then
						this_p_code = "28";
					elseif( model_num == 17 ) then
						this_p_code = "29";
					elseif( model_num == 18 ) then
						this_p_code = "30";
					elseif( model_num == 19 ) then
						this_p_code = "31";
					elseif( model_num == 20 ) then
						this_p_code = "32";
					end;
				elseif( stim_emotion == 3 ) then # fearful Young setB faces
					if( model_num == 17 ) then
						this_p_code = "41";
					elseif( model_num == 18 ) then
						this_p_code = "42";
					elseif( model_num == 19 ) then
						this_p_code = "43";
					elseif( model_num == 20 ) then
						this_p_code = "44";
					elseif( model_num == 21 ) then
						this_p_code = "45";
					elseif( model_num == 22 ) then
						this_p_code = "46";
					elseif( model_num == 23 ) then
						this_p_code = "47";
					elseif( model_num == 24 ) then
						this_p_code = "48";
					end;
				end;
			else#if( stim_age == 2 ) then # Old setB faces
				if( stim_emotion == 1 ) then # neutral Old setB faces
					if( model_num == 13 ) then
						this_p_code = "109";
					elseif( model_num == 14 ) then
						this_p_code = "110";
					elseif( model_num == 15 ) then
						this_p_code = "111";
					elseif( model_num == 16 ) then
						this_p_code = "112";
					elseif( model_num == 21 ) then
						this_p_code = "113";
					elseif( model_num == 22 ) then
						this_p_code = "114";
					elseif( model_num == 23 ) then
						this_p_code = "115";
					elseif( model_num == 24 ) then
						this_p_code = "116";
					end;
				elseif( stim_emotion == 2 ) then # angry Old setB faces
					if( model_num == 13 ) then
						this_p_code = "125";
					elseif( model_num == 14 ) then
						this_p_code = "126";
					elseif( model_num == 15 ) then
						this_p_code = "127";
					elseif( model_num == 16 ) then
						this_p_code = "128";
					elseif( model_num == 17 ) then
						this_p_code = "129";
					elseif( model_num == 18 ) then
						this_p_code = "130";
					elseif( model_num == 19 ) then
						this_p_code = "131";
					elseif( model_num == 20 ) then
						this_p_code = "132";
					end;
				else#( stim_emotion == 3 ) then # fearful Old setB faces
					if( model_num == 17 ) then
						this_p_code = "141";
					elseif( model_num == 18 ) then
						this_p_code = "142";
					elseif( model_num == 19 ) then
						this_p_code = "143";
					elseif( model_num == 20 ) then
						this_p_code = "144";
					elseif( model_num == 21 ) then
						this_p_code = "145";
					elseif( model_num == 22 ) then
						this_p_code = "146";
					elseif( model_num == 23 ) then
						this_p_code = "147";
					elseif( model_num == 24 ) then
						this_p_code = "148";
					end;
				end;
			end;
		end;
	else#if( stim_type == 2 ) then # scrambled
		if( set == "A" ) then # setA scrambled
			if( stim_age == 1 ) then # Young setA scrambled
				if( stim_emotion == 1 ) then # neutral Young setA scrambled
					if( model_num == 1 ) then
						this_p_code = "51";
					elseif( model_num == 2 ) then
						this_p_code = "52";
					elseif( model_num == 3 ) then
						this_p_code = "53";
					elseif( model_num == 4 ) then
						this_p_code = "54";
					elseif( model_num == 9 ) then
						this_p_code = "55";
					elseif( model_num == 10 ) then
						this_p_code = "56";
					elseif( model_num == 11 ) then
						this_p_code = "57";
					elseif( model_num == 12 ) then
						this_p_code = "58";
					end;
				elseif( stim_emotion == 2 ) then # angry Young setA scrambled
					if( model_num == 1 ) then
						this_p_code = "67";
					elseif( model_num == 2 ) then
						this_p_code = "68";
					elseif( model_num == 3 ) then
						this_p_code = "69";
					elseif( model_num == 4 ) then
						this_p_code = "70";
					elseif( model_num == 5 ) then
						this_p_code = "71";
					elseif( model_num == 6 ) then
						this_p_code = "72";
					elseif( model_num == 7 ) then
						this_p_code = "73";
					elseif( model_num == 8 ) then
						this_p_code = "74";
					end;
				elseif( stim_emotion == 3 ) then # fearful Young setA scrambled
					if( model_num == 5 ) then
						this_p_code = "83";
					elseif( model_num == 6 ) then
						this_p_code = "84";
					elseif( model_num == 7 ) then
						this_p_code = "85";
					elseif( model_num == 8 ) then
						this_p_code = "86";
					elseif( model_num == 9 ) then
						this_p_code = "87";
					elseif( model_num == 10 ) then
						this_p_code = "88";
					elseif( model_num == 11 ) then
						this_p_code = "89";
					elseif( model_num == 12 ) then
						this_p_code = "90";
					end;
				end;
			else#if( stim_age == 2 ) then # Old set A scrambled
				if( stim_emotion == 1 ) then # neutral Old setA scrambled
					if( model_num == 1 ) then
						this_p_code = "151";
					elseif( model_num == 2 ) then
						this_p_code = "152";
					elseif( model_num == 3 ) then
						this_p_code = "153";
					elseif( model_num == 4 ) then
						this_p_code = "154";
					elseif( model_num == 9 ) then
						this_p_code = "155";
					elseif( model_num == 10 ) then
						this_p_code = "156";
					elseif( model_num == 11 ) then
						this_p_code = "157";
					elseif( model_num == 12 ) then
						this_p_code = "158";
					end;
				elseif( stim_emotion == 2 ) then # angry Old setA scrambled
					if( model_num == 1 ) then
						this_p_code = "167";
					elseif( model_num == 2 ) then
						this_p_code = "168";
					elseif( model_num == 3 ) then
						this_p_code = "169";
					elseif( model_num == 4 ) then
						this_p_code = "170";
					elseif( model_num == 5 ) then
						this_p_code = "171";
					elseif( model_num == 6 ) then
						this_p_code = "172";
					elseif( model_num == 7 ) then
						this_p_code = "173";
					elseif( model_num == 8 ) then
						this_p_code = "174";
					end;
				else#if( stim_emotion == 3 ) then # fearful Old setA scrambled
					if( model_num == 5 ) then
						this_p_code = "183";
					elseif( model_num == 6 ) then
						this_p_code = "184";
					elseif( model_num == 7 ) then
						this_p_code = "185";
					elseif( model_num == 8 ) then
						this_p_code = "186";
					elseif( model_num == 9 ) then
						this_p_code = "187";
					elseif( model_num == 10 ) then
						this_p_code = "188";
					elseif( model_num == 11 ) then
						this_p_code = "189";
					elseif( model_num == 12 ) then
						this_p_code = "190";
					end;
				end;
				
			end;
		else#if( set == "B" ) then # setB scrambled
			if( stim_age == 1 ) then # Young setB scrambled
				if( stim_emotion == 1 ) then # neutral Young setB scrambled
					if( model_num == 13 ) then
						this_p_code = "59";
					elseif( model_num == 14 ) then
						this_p_code = "60";
					elseif( model_num == 15 ) then
						this_p_code = "61";
					elseif( model_num == 16 ) then
						this_p_code = "62";
					elseif( model_num == 21 ) then
						this_p_code = "63";
					elseif( model_num == 22 ) then
						this_p_code = "64";
					elseif( model_num == 23 ) then
						this_p_code = "65";
					elseif( model_num == 24 ) then
						this_p_code = "66";
					end;
				elseif( stim_emotion == 2 ) then # angry Young setB scrambled
					if( model_num == 13 ) then
						this_p_code = "75";
					elseif( model_num == 14 ) then
						this_p_code = "76";
					elseif( model_num == 15 ) then
						this_p_code = "77";
					elseif( model_num == 16 ) then
						this_p_code = "78";
					elseif( model_num == 17 ) then
						this_p_code = "79";
					elseif( model_num == 18 ) then
						this_p_code = "80";
					elseif( model_num == 19 ) then
						this_p_code = "81";
					elseif( model_num == 20 ) then
						this_p_code = "82";
					end;
				else#if( stim_emotion == 3 ) then # fearful Young setB scrambled
					if( model_num == 17 ) then
						this_p_code = "91";
					elseif( model_num == 18 ) then
						this_p_code = "92";
					elseif( model_num == 19 ) then
						this_p_code = "93";
					elseif( model_num == 20 ) then
						this_p_code = "94";
					elseif( model_num == 21 ) then
						this_p_code = "95";
					elseif( model_num == 22 ) then
						this_p_code = "96";
					elseif( model_num == 23 ) then
						this_p_code = "97";
					elseif( model_num == 24 ) then
						this_p_code = "98";
					end;
				end;
			else#if( stim_age == 2 ) then # Old setB scrambled
				if( stim_emotion == 1 ) then # neutral Old setB scrambled
					if( model_num == 13 ) then
						this_p_code = "159";
					elseif( model_num == 14 ) then
						this_p_code = "160";
					elseif( model_num == 15 ) then
						this_p_code = "161";
					elseif( model_num == 16 ) then
						this_p_code = "162";
					elseif( model_num == 21 ) then
						this_p_code = "163";
					elseif( model_num == 22 ) then
						this_p_code = "164";
					elseif( model_num == 23 ) then
						this_p_code = "165";
					elseif( model_num == 24 ) then
						this_p_code = "166";
					end;
				elseif( stim_emotion == 2 ) then # angry Old setB scrambled
					if( model_num == 13 ) then
						this_p_code = "175";
					elseif( model_num == 14 ) then
						this_p_code = "176";
					elseif( model_num == 15 ) then
						this_p_code = "177";
					elseif( model_num == 16 ) then
						this_p_code = "178";
					elseif( model_num == 17 ) then
						this_p_code = "179";
					elseif( model_num == 18 ) then
						this_p_code = "180";
					elseif( model_num == 19 ) then
						this_p_code = "181";
					elseif( model_num == 20 ) then
						this_p_code = "182";
					end;
				else#if( stim_emotion == 3 ) then # fearful Old setB scrambled
					if( model_num == 17 ) then
						this_p_code = "191";
					elseif( model_num == 18 ) then
						this_p_code = "192";
					elseif( model_num == 19 ) then
						this_p_code = "193";
					elseif( model_num == 20 ) then
						this_p_code = "194";
					elseif( model_num == 21 ) then
						this_p_code = "195";
					elseif( model_num == 22 ) then
						this_p_code = "196";
					elseif( model_num == 23 ) then
						this_p_code = "197";
					else#if( model_num == 24 ) then
						this_p_code = "198";
					end;
				end;
			end;
		end;
	end;
	#term.print("\nPCODE: "+this_p_code+"\n");
	#term.print( string(stim_type) + " " + string(stim_age) + " " + string(stim_emotion) + " " + string(model_num) + " " + set );
	return int(this_p_code);
end;

# Initialize some other values
int trials_per_rest = /*2460*/ parameter_manager.get_int( "Trials Between Rest Breaks" );
array<int> ISI_range[2];
parameter_manager.get_ints( "ISI Range", ISI_range );
if ( ISI_range.count() != 2 ) then
	exit( "Error: Exactly two values must be specified in 'ISI Range'" );
end;

# Get the requested stimulus durations, exit if none
int stim_dur = parameter_manager.get_int( "Stimulus Duration" );
trial_refresh_fix( stim_trial, stim_dur );

# Instructions
string instructions = "In this experiment, you will complete two tasks where you will be presented with images of faces presenting three different emotion expressions.";
instructions = instructions + "\n\nThe emotions will be one of:\nAngry, Fearful, and Neutral (neutral is when a face shows no particular emotion).\n";
instructions = instructions + "\n\nIn one task, you will be asked to respond based on the identity of the presented faces.\n\n";
instructions = instructions + "In the other task, you will be asked to respond based on the emotion expression presented on the face image.\n\n";
instructions = instructions + "Please take your time and read carefully the directions presented before each task. \n\n Press the top-right trigger to continue.";

# string reminder_cap = "Reminder: Please try to respond faster. \n\n Press the top-right trigger wehn you are ready to continue.";

# Set some captions
string start_caption = "We are about to begin.";
string prac_caption = "We will now start with practice.";
string complete_caption = "Session completed. Thank you for your patience and time!";
# ----------------------------------------------- PRACTICE TRIAL ------------------------------------------ #
array<int> prac_W_cond[1][15][5];
array<int> prac_B_cond[1][15][5];
# Build a practice trial sequence

	array<int> temp_prac_Old[1][0][0];
	array<int> temp_prac_Young[1][0][0];
	loop
		int yep = 1
	until
		yep > 15
	begin
	loop
		int i = 1
	until
		i > 2
	begin
		loop
			int j = 1
		until
			j > 2
		begin
			loop
				int k = 1
			until
				k > 3
			begin
				loop
					int s = 1
				until
					s > 6
				begin
					array<int> temp[5];
					temp[age_IDX] = i;
					temp[FACE_OR_SCRAM_IDX] = j;
					temp[EMOTION_IDX] = k;
					temp[MODEL_IDX] = s;		# if(prac_cond_array[i][5] == 1) then: REPEAT SAME STIMULI AGAIN
					temp[5] = 0; 				# temp[5] = 1 ==>> YES EMOTION
													# temp[5] = 2 ==>> YES face
													# temp[5] = 3 ==>> YES scrambled
					if(i == 1) then
						if( (yep == 1) && (j == 1 && k == 2) && s == 1 ) then
							temp_prac_Young[1].add( temp );
						elseif( (yep == 2) && j == 1 && k == 3 && s == 1 ) then
							temp[5] = 2;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 3) && j == 1 && k == 3 && s == 3 ) then
							temp[5] = 1;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 4) && j == 1 && k == 1 && s == 3 ) then
							temp[5] = 2;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 5) && j == 2 && k == 2 && s == 1 ) then
							temp_prac_Young[1].add( temp );
						elseif( (yep == 6) && j == 2 && k == 2 && s == 1 ) then
							temp[5] = 3;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 7) && j == 1 && k == 2 && s == 3 ) then
							temp_prac_Young[1].add( temp );
						elseif( (yep == 8) && j == 1 && k == 2 && s == 2 ) then
							temp[5] = 1;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 9) && j == 1 && k == 1 && s == 2 ) then
							temp[5] = 2;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 10) && j == 1 && k == 1 && s == 1 ) then
							temp[5] = 1;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 11) && j == 1 && k == 2 && s == 1 ) then
							temp[5] = 2;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 12) && j == 1 && k == 3 && s == 2 ) then
							temp_prac_Young[1].add( temp );
						elseif( (yep == 13) && j == 1 && k == 3 && s == 1 ) then
							temp[5] = 1;
							temp_prac_Young[1].add( temp );
						elseif( (yep == 14) && j == 2 && k == 1 && s == 2 ) then
							temp_prac_Young[1].add( temp );
						elseif( (yep == 15) && j == 2 && k == 1 && s == 2 ) then
							temp[5] = 3;
							temp_prac_Young[1].add( temp );
						end;
					elseif( i == 2 ) then
						if( (yep == 1) && (j == 1 && k == 1) && s == 4 ) then
							temp_prac_Old[1].add( temp );
						elseif( (yep == 2) && j == 1 && k == 1 && s == 6 ) then
							temp[5] = 1;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 3) && j == 1 && k == 3 && s == 6 ) then
							temp[5] = 2;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 4) && j == 1 && k == 3 && s == 5 ) then
							temp[5] = 1;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 5) && j == 2 && k == 1 && s == 6 ) then
							temp_prac_Old[1].add( temp );
						elseif( (yep == 6) && j == 2 && k == 1 && s == 6 ) then
							temp[5] = 3;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 7) && j == 1 && k == 1 && s == 5 ) then
							temp_prac_Old[1].add( temp );
						elseif( (yep == 8) && j == 1 && k == 2 && s == 5 ) then
							temp[5] = 2;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 9) && j == 1 && k == 2 && s == 4 ) then
							temp[5] = 1;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 10) && j == 1 && k == 3 && s == 4 ) then
							temp[5] = 2;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 11) && j == 2 && k == 3 && s == 4 ) then
							temp_prac_Old[1].add( temp );
						elseif( (yep == 12) && j == 2 && k == 3 && s == 4 ) then
							temp[5] = 3;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 13) && j == 1 && k == 2 && s == 5 ) then
							temp_prac_Old[1].add( temp );
						elseif( (yep == 14) && j == 2 && k == 2 && s == 6 ) then
							temp[5] = 1;
							temp_prac_Old[1].add( temp );
						elseif( (yep == 15) && j == 2 && k == 1 && s == 6 ) then
							temp[5] = 2;
							temp_prac_Old[1].add( temp );
						end;
					end;
					s = s + 1;
				end;
				k = k + 1;
			end;
			j = j + 1;
		end;
		i = i + 1;
	end;
	yep = yep + 1;
end;
prac_W_cond = temp_prac_Young;
prac_B_cond = temp_prac_Old;
#randomize order
prac_W_cond.shuffle();
prac_B_cond.shuffle();

# --------------------------------------------------------------------------------------- #
# Creating order for practice images
sub
	create_prac_order( array<int,3> arr )
begin
	arr[1].shuffle();
	bool track = false;
	loop
		int i = 1
	until
		i > 15
	begin
		track = false;
		array<int> temp[5];
		temp[1] = arr[1][i][1];
		temp[2] = arr[1][i][2];
		temp[3] = arr[1][i][3];
		temp[4] = arr[1][i][4];
		temp[5] = arr[1][i][5];
		array<int> replacement[5];
		if( i < 15 ) then
			replacement[1] = arr[1][i+1][1];
			replacement[2] = arr[1][i+1][2];
			replacement[3] = arr[1][i+1][3];
			replacement[4] = arr[1][i+1][4];
			replacement[5] = arr[1][i+1][5];
		end;
		if( i == 1 ) then #first image
			if( temp[5] == 1 ) then
				arr[1].shuffle();
				continue;
			end;
			arr[1][i] = temp;
			i = i + 1;
		elseif( i > 1 ) then # not first image
			if( temp[5] == 1 ) then # if repeating
				loop
					int j = i
				until
					j > (15 - i)
				begin
					if((arr[1][j][1] == temp[1])&&(arr[1][j][2] == temp[2])&&(arr[1][j][3] == temp[3])&&(arr[1][j][4] == temp[4])&&(arr[1][j][5] == temp[5])) then
						arr[1][i+1][1] = arr[1][j][1]; arr[1][i+1][2] = arr[1][j][2];
						arr[1][i+1][3] = arr[1][j][3]; arr[1][i+1][4] = arr[1][j][4];
						arr[1][i+1][5] = arr[1][j][5];
						arr[1][j] = replacement;
						arr[1][i][5] = 0;
						i = i + 2;
						term.print( "Got here. i: " );
						term.print( i );
						term.print( ":: j: " );
						term.print_line( j );
						j = 20;
					end;
					j = j + 1;
				end;
			elseif( temp[5] != 1 ) then # if not a repeat
				arr[1][i] = temp;
				i = i + 1;
				
			end;
		end;
	end;
end;
# =-=-=-=-=-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Main Sequence =-=-=-=-=-~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# setting my captions #
string face_prac_caption = "If there's a face you see twice, press: ";
string emotion_prac_caption = "If two faces have the same emotion in a row, press: ";
string face_start_caption = "...";
string emotion_start_caption =  "...";
string emotion = "Emotion";
string face = "Face";

# SETUP INSTRUCTIONS #
string temp0001 = "In this task you will be presented with images of faces presenting three different emotion expressions.\n";
string temp00011 = "The emotions will be one of:\nAngry, Fearful, and Neutral (neutral is when a face shows no particular emotion).\n";
string temp0002 = "Your task is to respond if the emotion expression presented on the face of the current image is the same as the emotion expression presented on the face of the previous image, regardless of the identity of the individual in the image.";
string temp0003 = "\n\n If the emotion expression presented on the face of the current image is the same as the emotion expression presented on the face of the previous image, press the top-right trigger. \n\n Please respond as quickly and accurately as possible. \n\n Press the top-right trigger to begin the practice trials.";
string P_EM = temp0001+temp00011+temp0002+temp0003;
string P_FA = "In this task you will be presented with images of faces presenting different emotion expressions. Your task is to respond if the face you are presented is of the same individual from the last image, regardless of the emotion expression presented in either image. \n\n If the face is of the same individual as the previously presented image, press the top-right trigger. \n\n Please respond as quickly and as accurately as possible. \n\n Press the top-right trigger to begin the practice trials."; 
string EM = "Practice trials completed. \n\n Please wait for the experimenter."; # 2700
string FA = "Practice trials completed! \n\n Please wait for the experimenter."; # 2701
# EMOTION NAMES #
array<string> emotion_names[3];
emotion_names[NEUTRAL] 	= "Neutral";
emotion_names[ANGRY] 	= "Angry";
emotion_names[FEARFUL] 	= "Fearful";

# HAVE THIS:  array<int> all_info[4][3][76][5];
all_info[1].shuffle();
all_info[2].shuffle();
all_info[3].shuffle();
all_info[4].shuffle();
array<int> WA[3][76][5] = all_info[1];
array<int> WB[3][76][5] = all_info[2];
array<int> BA[3][76][5] = all_info[3];
array<int> BB[3][76][5] = all_info[4];
# SETUP COUNTERBALANCE ORDER OF IMAGE ARRAYS AND INSTRUCTIONS #
array<int> aka[16][4];
#array<string> instr[16][4];


if(block_config == 1) then
	block_order[1][1] = prac_W_cond; block_order[1][2] = WB; instr[1][1] = P_EM; instr[1][2] = EM;
	block_order[1][3] = prac_B_cond; block_order[1][4] = BB; instr[1][3] = P_FA; instr[1][4] = FA;
	block_order[1][5] = prac_B_cond; block_order[1][6] = BB; aka[1][1] = 1; aka[1][2] = 2;
	block_order[1][7] = prac_W_cond; block_order[1][8] = WB; aka[1][3] = 2; aka[1][4] = 1;
elseif(block_config == 2) then
	block_order[2][1] = prac_W_cond; block_order[2][2] = WA; instr[2][1] = P_EM; instr[2][2] = EM; ##
	block_order[2][3] = prac_B_cond; block_order[2][4] = BA; instr[2][3] = P_FA; instr[2][4] = FA;
	block_order[2][5] = prac_B_cond; block_order[2][6] = BA; aka[2][1] = 1; aka[2][2] = 2;
	block_order[2][7] = prac_W_cond; block_order[2][8] = WA; aka[2][3] = 2; aka[2][4] = 1;
elseif(block_config == 3) then
	block_order[3][1] = prac_B_cond; block_order[3][2] = BB; instr[3][1] = P_FA; instr[3][2] = FA;   ########
	block_order[3][3] = prac_W_cond; block_order[3][4] = WB; instr[3][3] = P_EM; instr[3][4] = EM;
	block_order[3][5] = prac_W_cond; block_order[3][6] = WB; aka[3][1] = 2; aka[3][2] = 1;
	block_order[3][7] = prac_B_cond; block_order[3][8] = BB; aka[3][3] = 1; aka[3][4] = 2;
elseif(block_config == 4) then
	block_order[4][1] = prac_B_cond; block_order[4][2] = BA; instr[4][1] = P_FA; instr[4][2] = FA;    #######
	block_order[4][3] = prac_W_cond; block_order[4][4] = WA; instr[4][3] = P_EM; instr[4][4] = EM;
	block_order[4][5] = prac_W_cond; block_order[4][6] = WA; aka[4][1] = 2; aka[4][2] = 1;
	block_order[4][7] = prac_B_cond; block_order[4][8] = BA; aka[4][3] = 1; aka[4][4] = 2;
elseif(block_config == 5) then
	block_order[5][1] = prac_W_cond; block_order[5][2] = WB; instr[5][1] = P_FA; instr[5][2] = EM;
	block_order[5][3] = prac_B_cond; block_order[5][4] = BB; instr[5][3] = P_EM; instr[5][4] = FA;
	block_order[5][5] = prac_W_cond; block_order[5][6] = WB; aka[5][1] = 1; aka[5][2] = 2;
	block_order[5][7] = prac_B_cond; block_order[5][8] = BB; aka[5][3] = 1; aka[5][4] = 2;
elseif(block_config == 6) then
	block_order[6][1] = prac_W_cond; block_order[6][2] = WA; instr[6][1] = P_FA; instr[6][2] = FA;   ########
	block_order[6][3] = prac_B_cond; block_order[6][4] = BA; instr[6][3] = P_EM; instr[6][4] = EM;
	block_order[6][5] = prac_W_cond; block_order[6][6] = WA; aka[6][1] = 1; aka[6][2] = 2;
	block_order[6][7] = prac_B_cond; block_order[6][8] = BA; aka[6][3] = 1; aka[6][4] = 2;
elseif(block_config == 7) then
	block_order[7][1] = prac_W_cond; block_order[7][2] = WB; instr[7][1] = P_FA; instr[7][2] = FA;   ########
	block_order[7][3] = prac_B_cond; block_order[7][4] = BB; instr[7][3] = P_EM; instr[7][4] = EM;
	block_order[7][5] = prac_B_cond; block_order[7][6] = BB; aka[7][1] = 1; aka[7][2] = 2;
	block_order[7][7] = prac_W_cond; block_order[7][8] = WB; aka[7][3] = 2; aka[7][4] = 1;
elseif(block_config == 8) then
	block_order[8][1] = prac_B_cond; block_order[8][2] = BA; instr[8][1] = P_FA; instr[8][2] = FA;   ########
	block_order[8][3] = prac_W_cond; block_order[8][4] = WA; instr[8][3] = P_EM; instr[8][4] = EM;
	block_order[8][5] = prac_B_cond; block_order[8][6] = BA; aka[8][1] = 2; aka[8][2] = 1;
	block_order[8][7] = prac_W_cond; block_order[8][8] = WA; aka[8][3] = 2; aka[8][4] = 1;
elseif(block_config == 9) then
	block_order[9][1] = prac_B_cond; block_order[9][2] = BB; instr[9][1] = P_FA; instr[9][2] = FA;   ########
	block_order[9][3] = prac_W_cond; block_order[9][4] = WB; instr[9][3] = P_EM; instr[9][4] = EM;
	block_order[9][5] = prac_B_cond; block_order[9][6] = BB; aka[9][1] = 2; aka[9][2] = 1;
	block_order[9][7] = prac_W_cond; block_order[9][8] = WB; aka[9][3] = 2; aka[9][4] = 1;
elseif(block_config == 10) then
	block_order[10][1] = prac_W_cond; block_order[10][2] = WA; instr[10][1] = P_EM; instr[10][2] = EM;
	block_order[10][3] = prac_B_cond; block_order[10][4] = BA; instr[10][3] = P_FA; instr[10][4] = FA;
	block_order[10][5] = prac_W_cond; block_order[10][6] = WA; aka[10][1] = 1; aka[10][2] = 2;
	block_order[10][7] = prac_B_cond; block_order[10][8] = BA; aka[10][3] = 1; aka[10][4] = 2;
elseif(block_config == 11) then
	block_order[11][1] = prac_W_cond; block_order[11][2] = WB; instr[11][1] = P_EM; instr[11][2] = EM;
	block_order[11][3] = prac_B_cond; block_order[11][4] = BB; instr[11][3] = P_FA; instr[11][4] = FA;
	block_order[11][5] = prac_W_cond; block_order[11][6] = WB; aka[11][1] = 1; aka[11][2] = 2;
	block_order[11][7] = prac_B_cond; block_order[11][8] = BB; aka[11][3] = 1; aka[11][4] = 2;
elseif(block_config == 12) then
	block_order[12][1] = prac_B_cond; block_order[12][2] = BA; instr[12][1] = P_EM; instr[12][2] = EM;
	block_order[12][3] = prac_W_cond; block_order[12][4] = WA; instr[12][3] = P_FA; instr[12][4] = FA;
	block_order[12][5] = prac_B_cond; block_order[12][6] = BA; aka[12][1] = 2; aka[12][2] = 1;
	block_order[12][7] = prac_W_cond; block_order[12][8] = WA; aka[12][3] = 2; aka[12][4] = 1;
elseif(block_config == 13) then
	block_order[13][1] = prac_B_cond; block_order[13][2] = BB; instr[13][1] = P_EM; instr[13][2] = EM;
	block_order[13][3] = prac_W_cond; block_order[13][4] = WB; instr[13][3] = P_FA; instr[13][4] = FA;
	block_order[13][5] = prac_B_cond; block_order[13][6] = BB; aka[13][1] = 2; aka[13][2] = 1;
	block_order[13][7] = prac_W_cond; block_order[13][8] = WB; aka[13][3] = 2; aka[13][4] = 1;
elseif(block_config == 14) then
	block_order[14][1] = prac_B_cond; block_order[14][2] = BA; instr[14][1] = P_EM; instr[14][2] = EM;
	block_order[14][3] = prac_W_cond; block_order[14][4] = WA; instr[14][3] = P_FA; instr[14][4] = FA;
	block_order[14][5] = prac_W_cond; block_order[14][6] = WA; aka[14][1] = 2; aka[14][2] = 1;
	block_order[14][7] = prac_B_cond; block_order[14][8] = BA; aka[14][3] = 1; aka[14][4] = 2;
elseif(block_config == 15) then
	block_order[15][1] = prac_B_cond; block_order[15][2] = BB; instr[15][1] = P_EM; instr[15][2] = EM;
	block_order[15][3] = prac_W_cond; block_order[15][4] = WB; instr[15][3] = P_FA; instr[15][4] = FA;
	block_order[15][5] = prac_W_cond; block_order[15][6] = WB; aka[15][1] = 2; aka[15][2] = 1;
	block_order[15][7] = prac_B_cond; block_order[15][8] = BB; aka[15][3] = 1; aka[15][4] = 2;
elseif(block_config == 16) then
	block_order[16][1] = prac_W_cond; block_order[16][2] = WA; instr[16][1] = P_FA; instr[16][2] = FA;   ########
	block_order[16][3] = prac_B_cond; block_order[16][4] = BA; instr[16][3] = P_EM; instr[16][4] = EM;
	block_order[16][5] = prac_B_cond; block_order[16][6] = BA; aka[16][1] = 1; aka[16][2] = 2;
	block_order[16][7] = prac_W_cond; block_order[16][8] = WA; aka[16][3] = 2; aka[16][4] = 1;
end;
# Create Button-Code Arrays
array<int> face_yes_b_codes[1]; face_yes_b_codes[1] = 230;
array<int> face_b_codes[1]; face_b_codes[1] = 231;
array<int> emo_yes_b_codes[1]; emo_yes_b_codes[1] = 240;
array<int> emo_b_codes[1]; emo_b_codes[1] = 241;

# Determine if feedback is given after practice trials:
bool giveFeedback = false;
if( parameter_manager.get_bool( "Give Feedback After Practice" ) ) then
	giveFeedback = true;
end;
string newInstructions;
int yes_face = 4;
int yes_emo = 4;
int yes_scramb = 4;
int no_regular = 48;
int no_scramb = 16;
int G = block_config;
# --- sub show_trial_sequence					# this is just the definition of this function
sub
	show_trial_sequence( array<int,3>& trial_sequence, string prac_check )
begin
	# Get ready!
	ready_set_go();

	# Start with an ISI
	trial_refresh_fix( ISI_trial, random( ISI_range[1], ISI_range[2] ) );
	ISI_trial.present();
	
	# Determine number of trials in this block
	int length;
	int f_length;
	if		( prac_check == PRACTICE ) then
		length = 15;
		f_length = 2;		#3;#1;#2;
	elseif( prac_check == MAIN ) then
		length = 76;
		f_length = 4;     # n = 4 - 1 <---- what?
	end;
	int hits = 0; int misses = 0; int incorrects = 0;
	
       #######################################################################################################################################################
       #######################################################################################################################################################
       #       Loop
                                                                                                                                       #
       #######################################################################################################################################################
       #######################################################################################################################################################

	
	loop
		int f = 1			# for some reason, the "until" condition is the hard-break condition instead of the distant upper bound
	until	
		f == f_length
	begin
		# Loop to present trials
		array<int> previous[5];
		loop
			int i = 1
		until
			i > length
		begin	
			string task;
			string age;
			string set = "B";
			int stim_set;
		# Get Set info
		int tempVar = G; # int G = block_config;
		if( tempVar % 2 == 0 ) then   # A\ I hope modulus is not a mutative operation in this prog language
			set = "A";
		end;
/*
		# Get Task info																						# int G = block_config;
		if( task_number<2 && instr[G][2] == EM ) 		then      # somehow only either ONE or THREE are getting called
			task = "EmotionID"# "ONE" task_no=" + string(task_number)+" G="+string(G);# + " " + string(instr[G][2])+ " " + string(instr[G][4]);
		elseif( task_number<2 && instr[G][2] == FA ) then
			task = "FaceID";# "TWO" + string(task_number);# + " " + string(instr[G][2])+ " " + string(instr[G][4]);
		elseif( task_number>1 && instr[G][4] == EM ) then  # somehow only either ONE or THREE are getting called
			task = "EmotionID";# "THREE" task_no=" + string(task_number)+" G="+string(G);# + " " + string(instr[G][2]+ " " + string(instr[G][4]));
		elseif( task_number>1 && instr[G][4] == FA ) then
			task = "FaceID";# "FOUR" + string(task_number);# + " " + string(instr[G][2]+ " " + string(instr[G][4]));
		end;
*/

		if (G != 5) then
				  # Get Task info if you will                                                                                                                                                           # int G = block_config;
				  if    ( task_number<2 && instr[G][2] == EM ) then      # somehow only either ONE or THREE are getting called
							 task = "EmotionID"# "ONE" task_no=" + string(task_number)+" G="+string(G);# + " " + string(instr[G][2])+ " " + string(instr[G][4]);
				  elseif( task_number<2 && instr[G][2] == FA ) then
							 task = "FaceID";# "TWO" + string(task_number);# + " " + string(instr[G][2])+ " " + string(instr[G][4]);
				  elseif( task_number>1 && instr[G][4] == EM ) then  # somehow only either ONE or THREE are getting called
							 task = "EmotionID";# "THREE" task_no=" + string(task_number)+" G="+string(G);# + " " + string(instr[G][2]+ " " + string(instr[G][4]));
				  elseif( task_number>1 && instr[G][4] == FA ) then
							 task = "FaceID";# "FOUR" + string(task_number);# + " " + string(instr[G][2]+ " " + string(instr[G][4]));
				  end;
		else
				  if    ( task_number<2 && instr[G][2] == FA ) then      # somehow only either ONE or THREE are getting called
							 task = "EmotionID"# "ONE" task_no=" + string(task_number)+" G="+string(G);# + " " + string(instr[G][2])+ " " + string(instr[G][4]);
				  elseif( task_number<2 && instr[G][2] == EM ) then
							 task = "FaceID";# "TWO" + string(task_number);# + " " + string(instr[G][2])+ " " + string(instr[G][4]);
				  elseif( task_number>1 && instr[G][4] == FA ) then  # somehow only either ONE or THREE are getting called
							 task = "EmotionID";# "THREE" task_no=" + string(task_number)+" G="+string(G);# + " " + string(instr[G][2]+ " " + string(instr[G][4]));
				  elseif( task_number>1 && instr[G][4] == EM ) then
							 task = "FaceID";# "FOUR" + string(task_number);# + " " + string(instr[G][2]+ " " + string(instr[G][4]));
				  end;
		end;

		
		# Get age info
		if( aka[G][task_number + 1] == 1 ) then
			age = "Young";
		else age = "Old";
		end;
																							### int G = block_config;
/*			!!	REDUNDANT CODE BLOCK

		if( task_number == 0 && aka[G][1] == 1 ) then
			age = "Young";
		elseif( task_number == 0 && aka[G][1] == 2 ) then
			age = "Old";
		elseif( task_number == 1 && aka[G][2] == 1 ) then
			age = "Young";
		elseif( task_number == 1 && aka[G][2] == 2 ) then
			age = "Old";
		elseif( task_number == 2 && aka[G][3] == 1 ) then
			age = "Young";
		elseif( task_number == 2 && aka[G][3] == 2 ) then
			age = "Old";
		elseif( task_number == 3 && aka[G][4] == 1 ) then
			age = "Young";
		elseif( task_number == 3 && aka[G][4] == 2 ) then
			age = "Old";
		end;
*/
		# Get StimSet info
		if		( age == "Young" && set == "A" ) then
			stim_set = 1;
		elseif( age == "Young" && set == "B" ) then
			stim_set = 2;
		elseif( age == "Old" && set == "A" ) then
			stim_set = 3;
		elseif( age == "Old" && set == "B" ) then
			stim_set = 4;
		end;
			
			#supposed to & DO => HIT
			#supposed to responde & dont => MISS
			#respond & shouldn't => FALSE ALARM
			int this_type; 		#[1]
			int this_age; 		#[2]
			int this_emotion; 	#[3]
			int this_model_num; 	#[4]
			int this_repeat; 		#[5]
			# Get some values for this trial
			this_type = trial_sequence[f][i][1];
			this_age = trial_sequence[f][i][2];
			this_emotion = trial_sequence[f][i][3];
			this_model_num = trial_sequence[f][i][4];
			this_repeat = trial_sequence[f][i][5];
			
			string filename;
			if( prac_check == PRACTICE ) then
				#prac_bmps[this_age][this_type][this_emotion][this_model_num].load();
				filename = get_filename( prac_bmps[this_age][this_type][this_emotion][this_model_num] );
			else
			/*	term.print("stim_set: " );
				term.print( stim_set );
				term.print( ", this_type: ");
				term.print( this_type );
				term.print( ", this_emotion: ");
				term.print( this_emotion );
				term.print( ", this_model_num: " );
				term.print( this_model_num );
				term.print( ", this_repeat: " );
				term.print_line( this_repeat );
			*/	
				if( stim_set == 2 || stim_set == 4 ) then
					filename = get_filename( stim_sets[stim_set][this_type][this_emotion][(this_model_num+12)] );
				else
					filename = get_filename( stim_sets[stim_set][this_type][this_emotion][this_model_num] );
				end;
			end;
			
			int p_code = 0;
			
			# NEED TO SET TARGET BUTTONS & CODES
			if( prac_check == MAIN ) then
				#sound1.set_attenuation( 1 );
				if( task == "FaceID" ) then
					if( this_repeat == 2 ) then
						stim_event.set_target_button( 1 );
						response_manager.set_target_button_codes( face_yes_b_codes );
						p_code = 201;
					elseif( this_repeat == 3 ) then
						stim_event.set_target_button( 1 );
						response_manager.set_target_button_codes( face_yes_b_codes );
						p_code = 203;
					else
						stim_event.set_target_button( 0 );
					end;
					response_manager.set_button_codes( face_b_codes );
				elseif( task == "EmotionID" ) then
					if( this_repeat == 1 ) then
						stim_event.set_target_button( 1 );
						response_manager.set_target_button_codes( emo_yes_b_codes );
						p_code = 202;
					elseif( this_repeat == 3 ) then
						stim_event.set_target_button( 1 );
						response_manager.set_target_button_codes( emo_yes_b_codes );
						p_code = 203;
					else
						stim_event.set_target_button( 0 );
					end;
					response_manager.set_button_codes( emo_b_codes );
				end;
			elseif( prac_check == PRACTICE ) then
				if( task == "FaceID" ) then
					if( this_repeat == 2 || this_repeat == 3 ) then
						stim_event.set_target_button( 1 );
						#sound1.set_attenuation( 0.0 );
					else
						stim_event.set_target_button( 0 );
						#sound1.set_attenuation( 1.0 );
					end;
				elseif( task == "EmotionID" ) then
					if( this_repeat == 1 || this_repeat == 3 ) then
						stim_event.set_target_button( 1 );
						#sound1.set_attenuation( 0.0 );
					else
						stim_event.set_target_button( 0 );
						#sound1.set_attenuation( 1.0 );
					end;
				end;
			end;
		# Set the stimulus
			if ( prac_check == PRACTICE ) then
				stim_pic.set_part( 1, prac_bmps[this_age][this_type][this_emotion][this_model_num] );
			else
				if( stim_set == 2 || stim_set == 4 ) then
					stim_pic.set_part( 1, stim_sets[stim_set][this_type][this_emotion][(this_model_num+12)] );
				else
					stim_pic.set_part( 1, stim_sets[stim_set][this_type][this_emotion][this_model_num] );
				end;
			end;
		#stim_event.set_response_active( true );
		
		# Set the ISI duration
		int this_isi = random( ISI_range[1], ISI_range[2] );
		trial_refresh_fix( ISI_trial, this_isi );

		
		# Set port code if not already set
		if( int(p_code) == 0 || string(p_code) == "0" ) then
			if (G % 2 == 1) then
				p_code = get_port_code( this_type, this_age, this_emotion, this_model_num+12, set );
			else
				p_code = get_port_code( this_type, this_age, this_emotion, this_model_num, set );
			end;
		end;
		###################################
		stim_event.set_port_code( p_code );
		###################################
		string emo = emotion_names[this_emotion];
		# Get trial number 
		int n;
		n = i + ( 76 * task_number );
		string type;
		if( this_type == 1 ) then type = "Face";
		elseif( this_type == 2 ) then type = "Scrambled"; 
		end;
	
		# Set the event code
		stim_event.set_event_code(
			STIM_EVENT_CODE + ";" +
			prac_check + ";" +
			string( n ) + ";" +
			task + ";" +
			set + ";" +
			age + ";" +
			type + ";" + ####
			emo + ";" +
			string( this_model_num ) + ";" +
			string( this_isi ) + ";" +
			string( p_code ) + ";" +
			filename
		);
		
		# Show the trial
		stim_trial.present();
		# Show practice feedback
		stimulus_data last = stimulus_manager.last_stimulus_data();
		/*term.print_line( last.type() );
		term.print_line( "yep^^?" );
		term.print_line( string(stimulus_hit) );
		term.print_line( string(stimulus_incorrect) );
		term.print_line( string(stimulus_miss) );
		term.print_line( string(stimulus_false_alarm) );
		term.print_line( string(stimulus_other) );*/
		if (last.type() == stimulus_hit) then
			hits = hits + 1;
		elseif (last.type() == stimulus_miss) then
			misses = misses + 1;
		elseif (last.type() == stimulus_incorrect) then
			incorrects = incorrects + 1;
		end;
		string addToInstructions = "\n Hits: " + string(hits) + "\n Misses: " + string(misses) + "\n Incorrects: " + string(incorrects);
		if( task == "FaceID" ) then
			newInstructions = FA + addToInstructions;
		else
			newInstructions = EM + addToInstructions;
		end;
			# Show the ISI
		ISI_trial.present();
		# Show the rest
		if ( trials_per_rest > 0 ) && ( prac_check == MAIN ) then
			if ( i % 76 == 0 && f < 3 ) then
				rest_trial.present();
		#		present_instructions( reminder_cap );
				ready_set_go();
				yes_face = 4;
				yes_emo = 4;
				yes_scramb = 4;
				no_regular = 48;
				no_scramb = 16;
			end;
		end;
		
		# Save info about current trial image
		loop
			int p = 1
		until
			p > 5
		begin
			if( prac_check == MAIN && p == 5 ) then
				previous[5] = 0;
				p = p + 1;
				continue;
			end;
			previous[p] = trial_sequence[f][i][p];
			p = p + 1;
		end;
		if( prac_check == MAIN ) then
			term.print_line( this_model_num );
		end;
		# Increment
		i = i + 1;
	end;
	f = f + 1;
end;
end;
################################################################## MAIN SEQUENCE ##########################################

task_number = 0;

	#present_instructions( instructions );
	instruct_text.set_alpha( 0 );
	stimulus_holder_plane.set_size(display_device.custom_width(),display_device.custom_height());
	
	if (instr[G][1] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][1] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][1] ); #practice instructions
	show_trial_sequence( block_order[G][1], PRACTICE );
	
	if (instr[G][2] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][2] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][2] );
	show_trial_sequence( block_order[G][2], MAIN );
	
	instruct_text.set_alpha( 255 );
	stimulus_holder_plane.set_size(0,0);
	present_instructions( "End of block. Please wait for the experimenter before beginning the next one!" );
	instruct_text.set_alpha( 0 );
	stimulus_holder_plane.set_size(display_device.custom_width(),display_device.custom_height());
		
task_number = 1;

	instruct_text.set_alpha( 0 );
	stimulus_holder_plane.set_size(display_device.custom_width(),display_device.custom_height());
	
	if (instr[G][1] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][1] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][1] ); #practice instructions
	show_trial_sequence( block_order[G][3], PRACTICE );
	
	if (instr[G][2] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][2] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][2] );
	show_trial_sequence( block_order[G][4], MAIN );
	
	instruct_text.set_alpha( 255 );
	stimulus_holder_plane.set_size(0,0);
	present_instructions( "End of block. Please wait for the experimenter before beginning the next one!" );
	instruct_text.set_alpha( 0 );
	stimulus_holder_plane.set_size(display_device.custom_width(),display_device.custom_height());
		
task_number = 2;

	if (instr[G][3] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][3] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][3] ); #practice instructions
	show_trial_sequence( block_order[G][5], PRACTICE );
	
	if (instr[G][4] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][4] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][4] );
	show_trial_sequence( block_order[G][6], MAIN );
	
	instruct_text.set_alpha( 255 );
	stimulus_holder_plane.set_size(0,0);
	present_instructions( "End of block. Please wait for the experimenter before beginning the next one!" );
	instruct_text.set_alpha( 0 );
	stimulus_holder_plane.set_size(display_device.custom_width(),display_device.custom_height());
		
task_number = 3;

	if (instr[G][3] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][3] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][3] ); #practice instructions
	show_trial_sequence( block_order[G][7], PRACTICE );
	
	if (instr[G][4] == P_EM) then
		stimulus_holder_plane.load_texture("instr/emo1back.jpeg");
	elseif (instr[G][4] == P_FA) then
		stimulus_holder_plane.load_texture("instr/id1back.jpeg");
	end;
	present_instructions( instr[G][4] );
	show_trial_sequence( block_order[G][8], MAIN );
	
	instruct_text.set_alpha( 255 );
	stimulus_holder_plane.set_size(0,0);
	present_instructions( complete_caption );
	

/*
task_number = 0;

	present_instructions( instructions );
	present_instructions( instr[G][1] ); #practice instructions
		show_trial_sequence( block_order[G][1], PRACTICE );
	if( giveFeedback ) then
		present_instructions( newInstructions );
	else
		present_instructions( instr[G][2] );
	end;
		show_trial_sequence( block_order[G][2], MAIN );
		
		
task_number = 1;

	present_instructions( instr[G][1] ); #practice instructions
		show_trial_sequence( block_order[G][3], PRACTICE );
	if( giveFeedback ) then
		present_instructions( newInstructions );
	else
		present_instructions( instr[G][2] );
	end;
		show_trial_sequence( block_order[G][4], MAIN );
		
		
task_number = 2;

	present_instructions( instr[G][3] ); #practice instructions
		show_trial_sequence( block_order[G][5], PRACTICE );
	if( giveFeedback ) then
		present_instructions( newInstructions );
	else
		present_instructions( instr[G][4] );
	end;
		show_trial_sequence( block_order[G][6], MAIN );
		
		
task_number = 3;

	present_instructions( instr[G][3] ); #practice instructions
		show_trial_sequence( block_order[G][7], PRACTICE );
	if( giveFeedback ) then
		present_instructions( newInstructions );
	else
		present_instructions( instr[G][4] );
	end;
		show_trial_sequence( block_order[G][8], MAIN );
		
		
	present_instructions( complete_caption );

*/