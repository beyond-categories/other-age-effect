# Other Age Effect (OAE)

This repository houses the "Other Age Effect" set of experiments.

## Usage

To access files offline and to run experiments
- **Option 1**: clone repository using a terminal: `git clone https://gitlab.com/beyond-categories/other-age-effect.git` and `cd` into it
- **Option 2**: download this repository as [.zip](https://gitlab.com/beyond-categories/other-age-effect/-/archive/master/other-age-effect-master.zip) or 
[.tar.gz](https://gitlab.com/beyond-categories/other-age-effect/-/archive/master/other-age-effect-master.tar.gz).
Unzip or untar the compressed files into a directory of the same name (default).
Enter the directory and navigate to desired file.

Within each file, the paths to resources are relative, so experiments should work
irrespective of where the root folder (`other-age-effect`) is placed.

## Authors

All relevant experiment materials belong to their respective owners.
The code is believed to have been written by Wyatt J.
