# -------------------------- Header Parameters --------------------------
scenario = "ORE & Emotion";
write_codes = EXPARAM( "Send Port Codes" );
screen_width_distance = EXPARAM( "Display Width" );
screen_height_distance = EXPARAM( "Display Height" );
screen_distance = EXPARAM( "Viewing Distance" );
default_background_color = EXPARAM( "Background Color" );
default_font = EXPARAM( "Non-Stimulus Font" );
default_font_size = EXPARAM( "Non-Stimulus Font Size" );
default_text_color = EXPARAM( "Non-Stimulus Font Color" );
active_buttons = 2;
response_matching = simple_matching;
button_codes = 1,2;
target_button_codes = 102,101;
response_logging = EXPARAM( "Response Logging" );
stimulus_properties =
	event_name, string,
	trial_number, number,
	stim_age, string,
	stim_set, string,
	stim_model_num, string,
	ISI_duration, number,
	p_code, number,
	stim_file, string;
event_code_delimiter = ";";
# ------------------------------- SDL Part ------------------------------

begin;
trial{
	trial_type = first_response;
	trial_duration = forever;
	picture{
		plane { width=0.0; height=0.0; emissive=1.0,1.0,1.0; } stimulus_holder_plane;
                      x=0; y=0; z=0;
		text {
			caption = "Instructions";
			preload = false;
		} instruct_text;
		x = 0; 
		y = 0;
	} instruct_pic;
} instruct_trial;

/*
+trial{
+       trial_type = first_response;
+       trial_duration = forever;
+       picture{
+               plane { width=0.0; height=0.0; emissive=1.0,1.0,1.0; } stimulus_holder_plane;
+                       x=0; y=0; z=0;
+       } instructo_pic;
+} instructo_trial;
+*/


trial {
	stimulus_event {
		picture {} ISI_pic;
		code = "ISI";
	} ISI_event;
} ISI_trial;

# THREE EMOTION TRIALS

trial {
       #trial_duration = forever;
       #trial_type = first_response;
stimulus_event{
       picture{
               text{ caption = "Neutral?"; } emo_word_text;
               x = 0;
               y = 0;
       } emo_word_pic;
} emo_word_event;
} emo_word_trial;

# HAPPY_FACE_TRIAL
trial {
       #trial_duration = forever;
       #trial_type = first_response;
	stimulus_event{
       picture{
               bitmap{ filename = "this.jpg"; } happy_face_bmap;
               x = 0;
               y = 0;
       } happy_face_pic;
} happy_face_event;
} happy_face_trial;  


# STIM TRIAL
trial {

	clear_active_stimuli = false;
	stimulus_event {
		picture {
			ellipse_graphic {
				ellipse_height = EXPARAM( "Fixation Point Size" );
				ellipse_width = EXPARAM( "Fixation Point Size" );
				color = EXPARAM( "Fixation Point Color" );
			} fix_circ;
			x = 0;
			y = 0;
		} stim_pic;
		response_active = true;
		code = "Stim";
	} stim_event;
} stim_trial;

trial {
	stimulus_event {
		picture {
			text {
				caption = "Ready";
				preload = false;
			} ready_text;
			x = 0;
			y = 0;
		};
	} ready_event;
} ready_trial;

# ======================= CREATE STIMULUS ARRAYS ======================== #
		# young Set A
bitmap{ filename = "YF_A025_HAP.bmp"; } C_1_Hap;
bitmap{ filename = "YF_A049_HAP.bmp"; } C_2_Hap;
bitmap{ filename = "YF_A066_HAP.bmp"; } C_3_Hap;
bitmap{ filename = "YF_A114_HAP.bmp"; } C_4_Hap;
bitmap{ filename = "YF_A016_HAP.bmp"; } C_5_Hap;
bitmap{ filename = "YF_A031_HAP.bmp"; } C_6_Hap;
bitmap{ filename = "YF_A037_HAP.bmp"; } C_7_Hap;
bitmap{ filename = "YF_A089_HAP.bmp"; } C_8_Hap;
bitmap{ filename = "YF_A013_HAP.bmp"; } C_9_Hap;
bitmap{ filename = "YF_A062_HAP.bmp"; } C_10_Hap;
bitmap{ filename = "YF_A081_HAP.bmp"; } C_11_Hap;
bitmap{ filename = "YF_A105_HAP.bmp"; } C_12_Hap;
		# young Set B
bitmap{ filename = "YF_B025_HAP.bmp"; } C_13_Hap;
bitmap{ filename = "YF_B049_HAP.bmp"; } C_14_Hap;
bitmap{ filename = "YF_B066_HAP.bmp"; } C_15_Hap;
bitmap{ filename = "YF_B114_HAP.bmp"; } C_16_Hap;
bitmap{ filename = "YF_B016_HAP.bmp"; } C_17_Hap;
bitmap{ filename = "YF_B031_HAP.bmp"; } C_18_Hap;
bitmap{ filename = "YF_B037_HAP.bmp"; } C_19_Hap;
bitmap{ filename = "YF_B089_HAP.bmp"; } C_20_Hap;
bitmap{ filename = "YF_B013_HAP.bmp"; } C_21_Hap;
bitmap{ filename = "YF_B062_HAP.bmp"; } C_22_Hap;
bitmap{ filename = "YF_B081_HAP.bmp"; } C_23_Hap;
bitmap{ filename = "YF_B105_HAP.bmp"; } C_24_Hap;
		# Old Set A
bitmap{ filename = "OF_A015_HAP.bmp"; } B_1_Hap;
bitmap{ filename = "OF_A033_HAP.bmp"; } B_2_Hap;
bitmap{ filename = "OF_A091_HAP.bmp"; } B_3_Hap;
bitmap{ filename = "OF_A131_HAP.bmp"; } B_4_Hap;
bitmap{ filename = "OF_A004_HAP.bmp"; } B_5_Hap;
bitmap{ filename = "OF_A083_HAP.bmp"; } B_6_Hap;
bitmap{ filename = "OF_A107_HAP.bmp"; } B_7_Hap;
bitmap{ filename = "OF_A118_HAP.bmp"; } B_8_Hap;
bitmap{ filename = "OF_A027_HAP.bmp"; } B_9_Hap;
bitmap{ filename = "OF_A065_HAP.bmp"; } B_10_Hap;
bitmap{ filename = "OF_A146_HAP.bmp"; } B_11_Hap;
bitmap{ filename = "OF_A151_HAP.bmp"; } B_12_Hap;
		# Old Set B
bitmap{ filename = "OF_B015_HAP.bmp"; } B_13_Hap;
bitmap{ filename = "OF_B033_HAP.bmp"; } B_14_Hap;
bitmap{ filename = "OF_B091_HAP.bmp"; } B_15_Hap;
bitmap{ filename = "OF_B131_HAP.bmp"; } B_16_Hap;
bitmap{ filename = "OF_B004_HAP.bmp"; } B_17_Hap;
bitmap{ filename = "OF_B083_HAP.bmp"; } B_18_Hap;
bitmap{ filename = "OF_B107_HAP.bmp"; } B_19_Hap;
bitmap{ filename = "OF_B118_HAP.bmp"; } B_20_Hap;
bitmap{ filename = "OF_B027_HAP.bmp"; } B_21_Hap;
bitmap{ filename = "OF_B065_HAP.bmp"; } B_22_Hap;
bitmap{ filename = "OF_B146_HAP.bmp"; } B_23_Hap;
bitmap{ filename = "OF_B151_HAP.bmp"; } B_24_Hap;

# ----------------------------- PCL Program -----------------------------


begin_pcl;

include_once "lib_visual_utilities.pcl";
include_once "lib_utilities.pcl";

# --- CONSTANTS --- #

string STIM_EVENT_CODE = "Stimulus";

string PRACTICE = "Practice";
string MAIN = "Main";

string LOG_ACTIVE = "log_active";

int young_NUM = 1;
int Old_NUM = 2;

int FACE_OR_SCRAM_IDX = 1;
int age_IDX = 2;
int EMOTION_IDX = 3;
int MODEL_IDX = 4;

int NEUTRAL = 1;
int ANGRY = 2;
int FEARFUL = 3;

int CORR_BUTTON = 201;
int INCORR_BUTTON = 202;

int PORT_CODE_PREFIX = 100;

string CHARACTER_WRAP = "Character";

# =-=-=-=-=-=-=-=-=-=-=--=-=-=-= Create Stimulus Arrays =-=-=-=-=-=-=-=-=-=-=-=-=-=-= #
array<bitmap> all_bitmaps[2][2][12];
# [1=young][1=A][mod #]
# [2=Old][2=B][mod #]
		# young Set A
all_bitmaps[1][1][1] = C_1_Hap;
all_bitmaps[1][1][2] = C_2_Hap;
all_bitmaps[1][1][3] = C_3_Hap;
all_bitmaps[1][1][4] = C_4_Hap;
all_bitmaps[1][1][5] = C_5_Hap;
all_bitmaps[1][1][6] = C_6_Hap;
all_bitmaps[1][1][7] = C_7_Hap;
all_bitmaps[1][1][8] = C_8_Hap;
all_bitmaps[1][1][9] = C_9_Hap;
all_bitmaps[1][1][10] = C_10_Hap;
all_bitmaps[1][1][11] = C_11_Hap;
all_bitmaps[1][1][12] = C_12_Hap;
		# young Set B
all_bitmaps[1][2][1] = C_13_Hap;
all_bitmaps[1][2][2] = C_14_Hap;
all_bitmaps[1][2][3] = C_15_Hap;
all_bitmaps[1][2][4] = C_16_Hap;
all_bitmaps[1][2][5] = C_17_Hap;
all_bitmaps[1][2][6] = C_18_Hap;
all_bitmaps[1][2][7] = C_19_Hap;
all_bitmaps[1][2][8] = C_20_Hap;
all_bitmaps[1][2][9] = C_21_Hap;
all_bitmaps[1][2][10] = C_22_Hap;
all_bitmaps[1][2][11] = C_23_Hap;
all_bitmaps[1][2][12] = C_24_Hap;
		# Old Set A
all_bitmaps[2][1][1] = B_1_Hap;
all_bitmaps[2][1][2] = B_2_Hap;
all_bitmaps[2][1][3] = B_3_Hap;
all_bitmaps[2][1][4] = B_4_Hap;
all_bitmaps[2][1][5] = B_5_Hap;
all_bitmaps[2][1][6] = B_6_Hap;
all_bitmaps[2][1][7] = B_7_Hap;
all_bitmaps[2][1][8] = B_8_Hap;
all_bitmaps[2][1][9] = B_9_Hap;
all_bitmaps[2][1][10] = B_10_Hap;
all_bitmaps[2][1][11] = B_11_Hap;
all_bitmaps[2][1][12] = B_12_Hap;
		# Old Set B
all_bitmaps[2][2][1] = B_13_Hap;
all_bitmaps[2][2][2] = B_14_Hap;
all_bitmaps[2][2][3] = B_15_Hap;
all_bitmaps[2][2][4] = B_16_Hap;
all_bitmaps[2][2][5] = B_17_Hap;
all_bitmaps[2][2][6] = B_18_Hap;
all_bitmaps[2][2][7] = B_19_Hap;
all_bitmaps[2][2][8] = B_20_Hap;
all_bitmaps[2][2][9] = B_21_Hap;
all_bitmaps[2][2][10] = B_22_Hap;
all_bitmaps[2][2][11] = B_23_Hap;
all_bitmaps[2][2][12] = B_24_Hap;

# =~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~  =~=~=~=~=~=~=~=~=~=~=~=~=~ #
# GET INFO ON SET THEY'VE SEEN

string setTheyveSeen = parameter_manager.get_string( "Set They've Seen" );
bool seenSetA = false;
if( setTheyveSeen == "A" ) then
	seenSetA = true;
else
	seenSetA = false;
end;

# Integer Array #
array<int> all_ints[48][3];
array<int> seen_ints[24][3];
array<int> combo_ints[72][4]; 			# FOR combo_ints[n][4] = (1 = Neutral, 2 = Angry, 3 = Fearful)
int addCounter = 1;
int seenCounter = 1;
int comboCounter = 1;
loop
	int f = 1
until
	f > 2
begin
	loop
		int p = 1
	until
		p > 2
	begin
		loop
			int i = 1
		until
			i > 12
		begin
			  array<int> temp[3]; array<int> comb[4];
			  temp[1] = f; comb[1] = f;
			  temp[2] = p; comb[2] = p;
			  temp[3] = i; comb[3] = i;
			  if( p == 2 ) then                               # if set is 'B'
						 temp[3] = i + 12;                       # because model no. for 'B' sets should be 13-24
			  end;
			  if(seenSetA && p == 1) then             # if seen set 'A', add those values
						 seen_ints[seenCounter] = temp;
						 comb[4] = 1; combo_ints[comboCounter] = comb; comboCounter = comboCounter + 1;
						 comb[4] = 2; combo_ints[comboCounter] = comb; comboCounter = comboCounter + 1;
						 comb[4] = 3; combo_ints[comboCounter] = comb; comboCounter = comboCounter + 1;
						 seenCounter = seenCounter + 1;
			  elseif(!seenSetA && p == 2) then        # if seen set 'B', add those values
						 seen_ints[seenCounter] = temp;
						 comb[4] = 1; combo_ints[comboCounter] = comb; comboCounter = comboCounter + 1;
						 comb[4] = 2; combo_ints[comboCounter] = comb; comboCounter = comboCounter + 1;
						 comb[4] = 3; combo_ints[comboCounter] = comb; comboCounter = comboCounter + 1;
						 seenCounter = seenCounter + 1;
			end;
			all_ints[addCounter] = temp;
			addCounter = addCounter + 1;
			i = i + 1;
		end;
		p = p + 1;
	end;
	f = f + 1;
end;
all_ints.shuffle(); seen_ints.shuffle(); combo_ints.shuffle();


# Emotion Names Array #
array<string> emo_names[3];
emo_names[1] = "Neutral?";
emo_names[2] = "Angry?";
emo_names[3] = "Fearful?";

# Randomize Emotion Order
array<string> emo_strings[0][3];
			# emo_strings has four instances of the six randomizations of the three emotions F-A-N 
loop									
	int f = 1
until
	f > 6
begin
	loop
		int i = 1
	until
		i > 4
	begin
		array<string> temp[3];
		i = i + 1;
		if( f == 1 ) then
			temp[1] = "Neutral?"; temp[2] = "Angry?"; temp[3] = "Fearful?";
		elseif( f == 2 ) then
			temp[1] = "Neutral?"; temp[2] = "Fearful?"; temp[3] = "Angry?";
		elseif( f == 3 ) then
			temp[1] = "Angry?"; temp[2] = "Neutral?"; temp[3] = "Fearful?";
		elseif( f == 4 ) then
			temp[1] = "Angry?"; temp[2] = "Fearful?"; temp[3] = "Neutral?";
		elseif( f == 5 ) then
			temp[1] = "Fearful?"; temp[2] = "Neutral?"; temp[3] = "Angry?";
		elseif( f == 6 ) then
			temp[1] = "Fearful?"; temp[2] = "Angry?"; temp[3] = "Neutral?";
		end;
		emo_strings.add( temp );
	end;
	f = f + 1;
end;
emo_strings.shuffle();

# --- Set up fixed stimulus parameters ---
bool char_wrap = false;
double font_size = parameter_manager.get_double( "Non-Stimulus Font Size" );

# Add fixation to ISI
if ( parameter_manager.get_bool( "Show Fixation Point During ISI" ) ) then
	ISI_pic.add_part( fix_circ, 0, 0 );
end;

# Change response logging
if ( parameter_manager.get_string( "Response Logging" ) == LOG_ACTIVE ) then
	ISI_trial.set_all_responses( false );
	stim_trial.set_all_responses( false );
end;

# --- Subroutines --- #
# --- sub get_port_code
sub
	int get_port_code( int this_model_num, int this_age, string this_set )
begin
	int this_p_code_int;
	if( this_age == 1 ) then # young
		if( this_set == "A" ) then # young SET A
			this_p_code_int = this_model_num;
		elseif( this_set == "B" ) then # Old SET B
			this_p_code_int = this_model_num + 12;
		end;
	elseif( this_age == 2 ) then # Old
		if( this_set == "A" ) then # Old SET A
			this_p_code_int = this_model_num + 24;
		elseif( this_set == "B" ) then # Old SET B
			this_p_code_int = this_model_num + 36;
		end;
	end;
	return this_p_code_int;
end;
# --- sub present_instructions 
sub
	present_instructions( string instruct_string )
begin
	full_size_word_wrap( instruct_string, font_size, char_wrap, instruct_text );
	instruct_trial.present();
	#default.present();
end;
# --- sub ready_set_go ---
int ready_dur = parameter_manager.get_int( "Ready-Set-Go Duration" );
trial_refresh_fix( ready_trial, ready_dur );
array<string> ready_caps[3];
ready_caps[1] = "Ready";
ready_caps[2] = "Set";
ready_caps[3] = "Go!";
sub
	ready_set_go
begin
	if ( ready_dur > 0 ) then
		loop
			int i = 1
		until
			i > ready_caps.count()
		begin
			full_size_word_wrap( ready_caps[i], font_size, char_wrap, ready_text );
			ready_trial.present();
			i = i + 1;
		end;
	end;
end;
# --- sub get_filename
sub
	string get_filename( bitmap this_bitmap )
begin
	string temp_string = this_bitmap.filename();
	int last_slash = 1;
	loop
	until
		temp_string.find( "\\", last_slash ) == 0
	begin
		last_slash = last_slash + 1;
	end;
	temp_string = temp_string.substring( last_slash, temp_string.count()-last_slash+1 );
	return temp_string
end;

# SETUP ISI RANGE
array<int> ISI_range[2];
parameter_manager.get_ints( "ISI Range", ISI_range );
if ( ISI_range.count() != 2 ) then
	exit( "Error: Exactly two values must be specified in 'ISI Range'" );
end;

# Get the requested stimulus durations, exit if none
int stim_dur = parameter_manager.get_int( "Stimulus Duration" );
trial_refresh_fix( stim_trial, stim_dur );

# Set some captions
string start_caption = "In this task, a series of face images will be presented in the center of the screen. For each image, determine if you saw it in the previous experiment. \n\n If you have seen the face before, press the top-right trigger.";
start_caption = start_caption + "\n If you have not seen the face before, press the bottom-right trigger. \n\n Please respond as quickly and accurately as possible. ";
string intertrial_break = "Please take a real break here! Please wait for the experimenter before you begin the next part of this experiment.";
string complete_caption = "You have now completed the experiment. Thank you for participating.";
string intertrial_caption = "Now you will be shown all of the faces which you have seen during the study phase. For each emotion, (angry, fearful, sad) answer if you have seen that face showing that emotion.";

# Create Button-Codes int Array
array<int> targ_b_codes[2]; targ_b_codes[1] = 102; targ_b_codes[2] = 101;
array<int> just_b_codes[2]; just_b_codes[1] = 103; just_b_codes[2] = 104;

# ##################   SUB SHOW_TRIAL_SEQUENCE   ################# #
sub
	show_trial_sequence( array<int,2>& trial_sequence )
begin
	# Get ready!
	ready_set_go();
	# Start with an ISI
	trial_refresh_fix( ISI_trial, random( ISI_range[1], ISI_range[2] ) );
	ISI_trial.present();
	loop
		int i = 1
	until
		i > 48
	begin
# GET SOME VALUES FOR THIS IMAGE 
		array<int> temp[3] = trial_sequence[i];
	# Set info
		string set; int setInt;
		if( temp[2] == 1 ) then
			set = "A"; setInt = 1;
		else
			set = "B"; setInt = 2;
		end;
	# age info
		string age; int ageInt;
		if( temp[1] == 1 ) then
			age = "young"; ageInt = 1;
		else
			age = "Old"; ageInt = 2;
		end;
		if( temp[3] > 12 ) then
			temp[3] = temp[3] - 12;
		end;
	# Model number & File name
		int model_number = temp[3];
		string filename = get_filename( all_bitmaps[temp[1]][temp[2]][temp[3]] );

# SET TARGET BUTTONS
		response_manager.set_target_button_codes( targ_b_codes );
		if( seenSetA ) then
			if( set == "B" ) then # NO >>> 
				stim_event.set_target_button( 1 );
				just_b_codes[1] = 102; just_b_codes[2] = 104;
				response_manager.set_button_codes( just_b_codes );
			elseif( set == "A" ) then # YES >>>
				stim_event.set_target_button( 2 );
				just_b_codes[1] = 101; just_b_codes[2] = 103;
				response_manager.set_button_codes( just_b_codes );
			end;
		elseif( !seenSetA ) then
			if( set == "A" ) then # NO >>>
				stim_event.set_target_button( 1 );
				just_b_codes[1] = 102; just_b_codes[2] = 104;
				response_manager.set_button_codes( just_b_codes );
			elseif( set == "B" ) then # YES >>>
				stim_event.set_target_button( 2 );
				just_b_codes[1] = 101; just_b_codes[2] = 103;
				response_manager.set_button_codes( just_b_codes );
			end;
		end;
		# Set the stimulus
		stim_pic.set_part( 1, all_bitmaps[ageInt][setInt][model_number] );
		
		# Set the ISI duration
		int this_isi = random( ISI_range[1], ISI_range[2] );
		trial_refresh_fix( ISI_trial, this_isi );

 		# Set port code
		int p_code = get_port_code( model_number, ageInt, set );
		stim_event.set_port_code( p_code );
		
		# Set the event code
		stim_event.set_event_code(
			STIM_EVENT_CODE + ";" +
			string( i ) + ";" +
			age + ";" +
			set + ";" +
			string( model_number ) + ";" +
			string( this_isi ) + ";" +
			string( p_code ) + ";" +
			filename
		);
		# Show the trials
		stim_trial.present();
		ISI_trial.present();
		# Increment
		i = i + 1;
	end;
end;

# ##################   SUB SHOW_STUDIED_SEQUENCE   ################# #
sub
	show_studied_sequence( array<int,2>& trial_sequence )
begin
	# Get ready!
	ready_set_go();
	# Start with an ISI
	trial_refresh_fix( ISI_trial, random( ISI_range[1], ISI_range[2] ) );
	ISI_trial.present();
	loop
		int i = 1
	until
		i > 72
	begin
		# GET SOME VALUES FOR THIS IMAGE 
		array<int> temp[4] = trial_sequence[i];
	# Set info
		string set; int setInt;
		if( temp[2] == 1 ) then
			set = "A"; setInt = 1;
		else
			set = "B"; setInt = 2;
		end;
	# age info
		string age; int ageInt;
		if( temp[1] == 1 ) then
			age = "young"; ageInt = 1;
		else
			age = "Old"; ageInt = 2;
		end;
		if( temp[3] > 12 ) then
			temp[3] = temp[3] - 12;			# NOT SURE ABOUT THIS <<<<<<<<< CHECK LATER ON
		end;
	# Model number and FileName
		int model_number = temp[3];
		string filename = get_filename( all_bitmaps[temp[1]][temp[2]][temp[3]] );
	# Emotion? for this trial
		int emo_int = temp[4];
		string FIRST;
		if( emo_int == 1 ) then FIRST = "Neutral?"; happy_face_event.set_port_code( 60 ); emo_word_event.set_port_code( 101 );
		elseif( emo_int == 2 ) then FIRST = "Angry?"; happy_face_event.set_port_code( 70 ); emo_word_event.set_port_code( 102 );
		elseif( emo_int == 3 ) then FIRST = "Fearful?"; happy_face_event.set_port_code( 80 ); emo_word_event.set_port_code( 103 );
		end;
		
# GET CORRECT EMOTIONS FOR THIS IMAGE
		array<string> correctEmos[2];
		if( model_number <= 4 ) then # seen neut and ang
			correctEmos[1] = "Neutral?"; correctEmos[2] = "Angry?";
		elseif( model_number >= 5 && model_number <= 8 ) then # seen ang & fear
			correctEmos[1] = "Angry?"; correctEmos[2] = "Fearful?";
		elseif( model_number >= 9 ) then # seen neut & fear
			correctEmos[1] = "Neutral?"; correctEmos[2] = "Fearful?";
		end;
# SET THE STIMULUS
		happy_face_pic.set_part( 1, all_bitmaps[ageInt][setInt][model_number] );

		bool first = false;
# SET THE THREE TRIAL CAPTIONS AND TARGET BUTTONS
		emo_word_text.set_caption( FIRST );
			if( FIRST == correctEmos[1] || FIRST == correctEmos[2] ) then
				happy_face_event.set_target_button( 2 );
				first = true;
			else
				happy_face_event.set_target_button( 1 );
			end;
		# REDRAW THREE TRIALS:
		emo_word_text.redraw();
		
		# Set the ISI duration
		int this_isi = random( 400, 600 );
		trial_refresh_fix( ISI_trial, this_isi );

# 	Set port code
		int p_code = get_port_code( model_number, ageInt, set );  			# DO WE WANT 'get_port_code' to take the emotion? as an argument
		happy_face_event.set_port_code( p_code );
		
		# Set the event code
		happy_face_event.set_event_code(
			STIM_EVENT_CODE + ";" +
			string( i ) + ";" +
			age + ";" +
			FIRST + ";" +
			set + ";" +
			string( model_number ) + ";" +
			string( this_isi ) + ";" +
			string( p_code ) + ";" +
			filename
		);
		
# SET EVENT CODES
																	# WHAT INFO DO WE WANT IN THE EVENT CODES????
																	string correct;
			if( first ) then correct = "Good";
			else correct = "noGood";
		end;
		emo_word_event.set_event_code(
			"Emo" + ";" +
			string( i ) + ";" +
			correct + ";" +
			FIRST
		);
# SET TRIAL DURATIONS
trial_refresh_fix( emo_word_trial, stim_dur );
trial_refresh_fix( happy_face_trial, stim_dur );

# SHOW EMO_WORD TRIAL
			if( first ) then # YES >>> 
				just_b_codes[1] = 203; just_b_codes[2] = 201;
				response_manager.set_button_codes( just_b_codes );
			else	# NO >>>
				just_b_codes[1] = 202; just_b_codes[2] = 204;
				response_manager.set_button_codes( just_b_codes );
			end;
			emo_word_trial.present();
			
# SHOW SHORTER ISI_TRIAL
		ISI_trial.present();
# SHOW HAPPY_FACE_TRIAL
		happy_face_trial.present();
		
		# Set the ISI duration
		this_isi = random( ISI_range[1], ISI_range[2] );
		trial_refresh_fix( ISI_trial, this_isi );
		ISI_trial.present();
		# Increment
		i = i + 1;
	end;
end;

	#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<#
	# 													MAIN SEQUENCE													#
	#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#


instruct_text.set_alpha( 0 );
stimulus_holder_plane.load_texture( "instr/id-mem.jpeg" );
stimulus_holder_plane.set_size( display_device.custom_width(),display_device.custom_height() );

present_instructions( start_caption );
show_trial_sequence( all_ints );

instruct_text.set_alpha( 255 );
stimulus_holder_plane.set_size( 0,0 );
present_instructions( intertrial_break );

instruct_text.set_alpha( 0 );
stimulus_holder_plane.load_texture( "instr/emo-mem.jpeg" );
stimulus_holder_plane.set_size( display_device.custom_width(),display_device.custom_height() );

present_instructions( intertrial_caption );    # show second task instructions
        # NOW PRESENT ONLY THE HAPPY FACES OF MODELS THEY HAVE SEEN,
                # ASK ABOUT ALL 3 EMOTIONS
show_studied_sequence( combo_ints );

instruct_text.set_alpha( 255 );
stimulus_holder_plane.set_size( 0,0 );
 present_instructions( complete_caption );
